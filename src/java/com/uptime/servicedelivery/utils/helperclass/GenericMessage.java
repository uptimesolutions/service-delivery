/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.servicedelivery.utils.helperclass;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author twilcox
 */
public class GenericMessage {

    /**
     * Service error message
     */
    public static void showErrorMsg() {
        //UserManageBean userManageBean;

        try {
            //if (userManageBeanCheck()) {
                //userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
                FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage("Something went wrong. Please contact IT."));
            //}
        } catch (Exception e) {
            Logger.getLogger(GenericMessage.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }
//
//    /**
//     * Successfully create item message
//     */
//    public static void createItemMsg() {
//        UserManageBean userManageBean;
//
//        try {
//            if (userManageBeanCheck()) {
//                userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
//                FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_create_item_items")));
//            }
//        } catch (Exception e) {
//            Logger.getLogger(GenericMessage.class.getName()).log(Level.SEVERE, e.getMessage(), e);
//        }
//    }
//
//    /**
//     * Successfully update item message
//     */
//    public static void updateItemMsg() {
//        UserManageBean userManageBean;
//
//        try {
//            if (userManageBeanCheck()) {
//                userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
//                FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_update_item_items")));
//            }
//        } catch (Exception e) {
//            Logger.getLogger(GenericMessage.class.getName()).log(Level.SEVERE, e.getMessage(), e);
//        }
//    }
//
//    /**
//     * Successfully delete item message
//     */
//    public static void deleteItemMsg() {
//        UserManageBean userManageBean;
//
//        try {
//            if (userManageBeanCheck()) {
//                userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
//                FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_delete_item_items")));
//            }
//        } catch (Exception e) {
//            Logger.getLogger(GenericMessage.class.getName()).log(Level.SEVERE, e.getMessage(), e);
//        }
//    }
//
//    /**
//     * Successful upload
//     */
//    public static void successfulUpload() {
//        UserManageBean userManageBean;
//
//        try {
//            if (userManageBeanCheck()) {
//                userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
//                FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_upload_item_items")));
//            }
//        } catch (Exception e) {
//            Logger.getLogger(GenericMessage.class.getName()).log(Level.SEVERE, e.getMessage(), e);
//        }
//    }
//
//    /**
//     * Upload failed
//     */
//    public static void failedUpload() {
//        UserManageBean userManageBean;
//
//        try {
//            if (userManageBeanCheck()) {
//                userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
//                FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_failed_upload_item_items")));
//            }
//        } catch (Exception e) {
//            Logger.getLogger(GenericMessage.class.getName()).log(Level.SEVERE, e.getMessage(), e);
//        }
//    }
//
//    /**
//     * Given name already in use
//     */
//    public static void nameAlreadyUsed() {
//        UserManageBean userManageBean;
//
//        try {
//            if (userManageBeanCheck()) {
//                userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
//                FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_name_already_in_use")));
//            }
//        } catch (Exception e) {
//            Logger.getLogger(GenericMessage.class.getName()).log(Level.SEVERE, e.getMessage(), e);
//        }
//    }
//
//    /**
//     * All field must be set
//     */
//    public static void missingFields() {
//        UserManageBean userManageBean;
//
//        try {
//            if (userManageBeanCheck()) {
//                userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
//                FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_all_field_must_be_set")));
//            }
//        } catch (Exception e) {
//            Logger.getLogger(GenericMessage.class.getName()).log(Level.SEVERE, e.getMessage(), e);
//        }
//    }
//
//    /**
//     * Trend data not available
//     */
//    public static void trendDataNotAvailable() {
//        UserManageBean userManageBean;
//
//        try {
//            if (userManageBeanCheck()) {
//                userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
//                FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage(userManageBean.getResourceBundleString("label_no_graph_data")));
//            }
//        } catch (Exception e) {
//            Logger.getLogger(GenericMessage.class.getName()).log(Level.SEVERE, e.getMessage(), e);
//        }
//    }
//
//    /**
//     * No changes found
//     */
//    public static void noChangesFound() {
//        UserManageBean userManageBean;
//
//        try {
//            if (userManageBeanCheck()) {
//                userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
//                FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_no_changes_found")));
//            }
//        } catch (Exception e) {
//            Logger.getLogger(GenericMessage.class.getName()).log(Level.SEVERE, e.getMessage(), e);
//        }
//    }
//
//    /**
//     * Output a growl message from the given resourceBundleString
//     *
//     * @param resourceBundleString, String Object
//     */
//    public static void showGrowlMsg(String resourceBundleString) {
//        UserManageBean userManageBean;
//
//        try {
//            if (userManageBeanCheck() && resourceBundleString != null && !resourceBundleString.isEmpty()) {
//                userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
//                FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage(userManageBean.getResourceBundleString(resourceBundleString)));
//            }
//        } catch (Exception e) {
//            Logger.getLogger(GenericMessage.class.getName()).log(Level.SEVERE, e.getMessage(), e);
//        }
//    }
//
//    /**
//     * Sets the non auto growl message with a message to be show upon update
//     * Note: This update needs to be performed in another part of the code
//     *
//     * @param resourceBundleString, String Object
//     */
//    public static void setNonAutoGrowlMsg(String resourceBundleString) {
//        UserManageBean userManageBean;
//
//        try {
//            if (userManageBeanCheck() && resourceBundleString != null && !resourceBundleString.isEmpty()) {
//                userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
//                FacesContext.getCurrentInstance().addMessage("nonAutoGrowlMessages", new FacesMessage(userManageBean.getResourceBundleString(resourceBundleString)));
//            }
//        } catch (Exception e) {
//            Logger.getLogger(GenericMessage.class.getName()).log(Level.SEVERE, e.getMessage(), e);
//        }
//    }
//
//    /**
//     * Used for multiple uploads to be combined into one message
//     *
//     * @param successCount, int
//     * @param errorCount, int
//     * @param alreadyExist, int
//     */
//    public static void failedSuccessUploads(int successCount, int errorCount, int alreadyExist) {
//        StringBuilder message;
//        UserManageBean userManageBean;
//
//        try {
//            if (userManageBeanCheck()){
//                userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
//                message = new StringBuilder();
//                if (successCount > 0) {
//                    message.append(successCount).append(" ").append(userManageBean.getResourceBundleString("growl_create_item_items"));
//                }
//                if (errorCount > 0) {
//                    message.append(successCount > 0 ? " " : "").append(errorCount).append(" ").append(userManageBean.getResourceBundleString("growl_failed_upload_item_items"));
//                }
//                if (alreadyExist > 0) {
//                    message.append(successCount > 0 || errorCount > 0 ? " " : "").append(alreadyExist).append(" ").append(userManageBean.getResourceBundleString("growl_exists_create_item_items"));
//                }
//                if (successCount <= 0 && errorCount <= 0 && alreadyExist <= 0) {
//                    message.append(userManageBean.getResourceBundleString("growl_no_items_created"));
//                }
//                FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage(message.toString()));
//            }
//        } catch (Exception e) {
//            Logger.getLogger(GenericMessage.class.getName()).log(Level.SEVERE, e.getMessage(), e);
//        }
//    }
//
//    /**
//     * Used for multiple uploads to be combined into one message
//     *
//     * @param successCount, int
//     * @param existsCount, int
//     * @param errorCount, int
//     * @param validationFailedCount, int
//     */
//    public static void failedSuccessUploads(int successCount, int existsCount, int errorCount, int validationFailedCount) {
//        StringBuilder message;
//        UserManageBean userManageBean;
//        int count = 0;
//        
//        try {
//            if (userManageBeanCheck()){
//                userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
//                message = new StringBuilder();
//                if (successCount > 0) {
//                    message.append(successCount).append(" ").append(userManageBean.getResourceBundleString("growl_create_item_items"));
//                    count++;
//                }
//                if (existsCount > 0) {
//                    if (count > 0) {
//                        message.append(" ");
//                    }
//                    message.append(existsCount).append(" ").append(userManageBean.getResourceBundleString("growl_exists_create_item_items"));
//                    count++;
//                }
//                if (errorCount > 0) {
//                    if (count > 0) {
//                        message.append(" ");
//                    }
//                    message.append(errorCount).append(" ").append(userManageBean.getResourceBundleString("growl_failed_upload_item_items"));
//                    count++;
//                }
//                if (validationFailedCount > 0) {
//                    if (count > 0) {
//                        message.append(" ");
//                    }
//                    message.append(validationFailedCount).append(" ").append(userManageBean.getResourceBundleString("growl_all_field_must_be_valid"));
//                }
//                if (successCount <= 0 && existsCount <= 0 && errorCount <= 0 && validationFailedCount <= 0) {
//                    message.append(userManageBean.getResourceBundleString("growl_no_items_created"));
//                }
//                FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage(message.toString()));
//            }
//        } catch (Exception e) {
//            Logger.getLogger(GenericMessage.class.getName()).log(Level.SEVERE, e.getMessage(), e);
//        }
//    }
//    
//    /**
//     * Check if the userManagedBean has been initiated 
//     * @return boolean, true if yes, else false
//     */
//    private static boolean userManageBeanCheck() {
//        try {
//            return FacesContext.getCurrentInstance() != null && 
//                    FacesContext.getCurrentInstance().getExternalContext() != null && 
//                    FacesContext.getCurrentInstance().getExternalContext().getSessionMap() != null && 
//                    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean") != null;
//        } catch (Exception e) {
//            Logger.getLogger(GenericMessage.class.getName()).log(Level.SEVERE, e.getMessage(), e);
//        }
//        return false;
//    }
}
