/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.servicedelivery.utils.vo;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author twilcox
 */
public class SiteCustomerVO implements Serializable {
    private String customerAccount;
    private UUID siteId;
    private String siteName;
    private String region;
    private String country;
    private String physicalAddress1;
    private String physicalAddress2;
    private String physicalCity;
    private String physicalStateProvince;
    private String physicalPostalCode;
    private String latitude;
    private String longitude;
    private String timezone;
    private String industry;
    private String shipAddress1;
    private String shipAddress2;
    private String shipCity;
    private String shipStateProvince;
    private String shipPostalCode;
    private String billingAddress1;
    private String billingAddress2;
    private String billingCity;
    private String billingStateProvince;
    private String billingPostalCode;

    public SiteCustomerVO() {
    }

    public SiteCustomerVO(SiteCustomerVO siteCustomerVO) {
        customerAccount = siteCustomerVO.getCustomerAccount();
        siteName = siteCustomerVO.getSiteName();
        siteId = siteCustomerVO.getSiteId();
        region = siteCustomerVO.getRegion();
        country = siteCustomerVO.getCountry();
        physicalAddress1 = siteCustomerVO.getPhysicalAddress1();
        physicalAddress2 = siteCustomerVO.getPhysicalAddress2();
        physicalCity = siteCustomerVO.getPhysicalCity();
        physicalStateProvince = siteCustomerVO.getPhysicalStateProvince();
        physicalPostalCode = siteCustomerVO.getPhysicalPostalCode();
        latitude = siteCustomerVO.getLatitude();
        longitude = siteCustomerVO.getLongitude();
        timezone = siteCustomerVO.getTimezone();
        industry = siteCustomerVO.getIndustry();
        shipAddress1 = siteCustomerVO.getShipAddress1();
        shipAddress2 = siteCustomerVO.getShipAddress2();
        shipCity = siteCustomerVO.getShipCity();
        shipStateProvince = siteCustomerVO.getShipStateProvince();
        shipPostalCode = siteCustomerVO.getShipPostalCode();
        billingAddress1 = siteCustomerVO.getBillingAddress1();
        billingAddress2 = siteCustomerVO.getBillingAddress2();
        billingCity = siteCustomerVO.getBillingCity();
        billingStateProvince = siteCustomerVO.getBillingStateProvince();
        billingPostalCode = siteCustomerVO.getBillingPostalCode();
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPhysicalAddress1() {
        return physicalAddress1;
    }

    public void setPhysicalAddress1(String physicalAddress1) {
        this.physicalAddress1 = physicalAddress1;
    }

    public String getPhysicalAddress2() {
        return physicalAddress2;
    }

    public void setPhysicalAddress2(String physicalAddress2) {
        this.physicalAddress2 = physicalAddress2;
    }

    public String getPhysicalCity() {
        return physicalCity;
    }

    public void setPhysicalCity(String physicalCity) {
        this.physicalCity = physicalCity;
    }

    public String getPhysicalStateProvince() {
        return physicalStateProvince;
    }

    public void setPhysicalStateProvince(String physicalStateProvince) {
        this.physicalStateProvince = physicalStateProvince;
    }

    public String getPhysicalPostalCode() {
        return physicalPostalCode;
    }

    public void setPhysicalPostalCode(String physicalPostalCode) {
        this.physicalPostalCode = physicalPostalCode;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getShipAddress1() {
        return shipAddress1;
    }

    public void setShipAddress1(String shipAddress1) {
        this.shipAddress1 = shipAddress1;
    }

    public String getShipAddress2() {
        return shipAddress2;
    }

    public void setShipAddress2(String shipAddress2) {
        this.shipAddress2 = shipAddress2;
    }

    public String getShipCity() {
        return shipCity;
    }

    public void setShipCity(String shipCity) {
        this.shipCity = shipCity;
    }

    public String getShipStateProvince() {
        return shipStateProvince;
    }

    public void setShipStateProvince(String shipStateProvince) {
        this.shipStateProvince = shipStateProvince;
    }

    public String getShipPostalCode() {
        return shipPostalCode;
    }

    public void setShipPostalCode(String shipPostalCode) {
        this.shipPostalCode = shipPostalCode;
    }

    public String getBillingAddress1() {
        return billingAddress1;
    }

    public void setBillingAddress1(String billingAddress1) {
        this.billingAddress1 = billingAddress1;
    }

    public String getBillingAddress2() {
        return billingAddress2;
    }

    public void setBillingAddress2(String billingAddress2) {
        this.billingAddress2 = billingAddress2;
    }

    public String getBillingCity() {
        return billingCity;
    }

    public void setBillingCity(String billingCity) {
        this.billingCity = billingCity;
    }

    public String getBillingStateProvince() {
        return billingStateProvince;
    }

    public void setBillingStateProvince(String billingStateProvince) {
        this.billingStateProvince = billingStateProvince;
    }

    public String getBillingPostalCode() {
        return billingPostalCode;
    }

    public void setBillingPostalCode(String billingPostalCode) {
        this.billingPostalCode = billingPostalCode;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.customerAccount);
        hash = 79 * hash + Objects.hashCode(this.siteId);
        hash = 79 * hash + Objects.hashCode(this.siteName);
        hash = 79 * hash + Objects.hashCode(this.region);
        hash = 79 * hash + Objects.hashCode(this.country);
        hash = 79 * hash + Objects.hashCode(this.physicalAddress1);
        hash = 79 * hash + Objects.hashCode(this.physicalAddress2);
        hash = 79 * hash + Objects.hashCode(this.physicalCity);
        hash = 79 * hash + Objects.hashCode(this.physicalStateProvince);
        hash = 79 * hash + Objects.hashCode(this.physicalPostalCode);
        hash = 79 * hash + Objects.hashCode(this.latitude);
        hash = 79 * hash + Objects.hashCode(this.longitude);
        hash = 79 * hash + Objects.hashCode(this.timezone);
        hash = 79 * hash + Objects.hashCode(this.industry);
        hash = 79 * hash + Objects.hashCode(this.shipAddress1);
        hash = 79 * hash + Objects.hashCode(this.shipAddress2);
        hash = 79 * hash + Objects.hashCode(this.shipCity);
        hash = 79 * hash + Objects.hashCode(this.shipStateProvince);
        hash = 79 * hash + Objects.hashCode(this.shipPostalCode);
        hash = 79 * hash + Objects.hashCode(this.billingAddress1);
        hash = 79 * hash + Objects.hashCode(this.billingAddress2);
        hash = 79 * hash + Objects.hashCode(this.billingCity);
        hash = 79 * hash + Objects.hashCode(this.billingStateProvince);
        hash = 79 * hash + Objects.hashCode(this.billingPostalCode);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SiteCustomerVO other = (SiteCustomerVO) obj;
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.siteName, other.siteName)) {
            return false;
        }
        if (!Objects.equals(this.region, other.region)) {
            return false;
        }
        if (!Objects.equals(this.country, other.country)) {
            return false;
        }
        if (!Objects.equals(this.physicalAddress1, other.physicalAddress1)) {
            return false;
        }
        if (!Objects.equals(this.physicalAddress2, other.physicalAddress2)) {
            return false;
        }
        if (!Objects.equals(this.physicalCity, other.physicalCity)) {
            return false;
        }
        if (!Objects.equals(this.physicalStateProvince, other.physicalStateProvince)) {
            return false;
        }
        if (!Objects.equals(this.physicalPostalCode, other.physicalPostalCode)) {
            return false;
        }
        if (!Objects.equals(this.latitude, other.latitude)) {
            return false;
        }
        if (!Objects.equals(this.longitude, other.longitude)) {
            return false;
        }
        if (!Objects.equals(this.timezone, other.timezone)) {
            return false;
        }
        if (!Objects.equals(this.industry, other.industry)) {
            return false;
        }
        if (!Objects.equals(this.shipAddress1, other.shipAddress1)) {
            return false;
        }
        if (!Objects.equals(this.shipAddress2, other.shipAddress2)) {
            return false;
        }
        if (!Objects.equals(this.shipCity, other.shipCity)) {
            return false;
        }
        if (!Objects.equals(this.shipStateProvince, other.shipStateProvince)) {
            return false;
        }
        if (!Objects.equals(this.shipPostalCode, other.shipPostalCode)) {
            return false;
        }
        if (!Objects.equals(this.billingAddress1, other.billingAddress1)) {
            return false;
        }
        if (!Objects.equals(this.billingAddress2, other.billingAddress2)) {
            return false;
        }
        if (!Objects.equals(this.billingCity, other.billingCity)) {
            return false;
        }
        if (!Objects.equals(this.billingStateProvince, other.billingStateProvince)) {
            return false;
        }
        if (!Objects.equals(this.billingPostalCode, other.billingPostalCode)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "SiteCustomerVO{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", siteName=" + siteName + ", region=" + region + ", country=" + country + ", physicalAddress1=" + physicalAddress1 + ", physicalAddress2=" + physicalAddress2 + ", physicalCity=" + physicalCity + ", physicalStateProvince=" + physicalStateProvince + ", physicalPostalCode=" + physicalPostalCode + ", latitude=" + latitude + ", longitude=" + longitude + ", timezone=" + timezone + ", industry=" + industry + ", shipAddress1=" + shipAddress1 + ", shipAddress2=" + shipAddress2 + ", shipCity=" + shipCity + ", shipStateProvince=" + shipStateProvince + ", shipPostalCode=" + shipPostalCode + ", billingAddress1=" + billingAddress1 + ", billingAddress2=" + billingAddress2 + ", billingCity=" + billingCity + ", billingStateProvince=" + billingStateProvince + ", billingPostalCode=" + billingPostalCode + '}';
    }
}
