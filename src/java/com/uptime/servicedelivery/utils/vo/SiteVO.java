/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.servicedelivery.utils.vo;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author kpati
 */
public class SiteVO implements Serializable {

    private String customerAccount;
    private UUID siteId;
    private String siteName;
    private String region;
    private String country;
    private String physicalAddress1;
    private String physicalAddress2;
    private String physicalCity;
    private String physicalStateProvince;
    private String physicalPostalCode;
    private String latitude;
    private String longitude;
    private String timezone;
    private String industry;
    private String shipAddress1;
    private String shipAddress2;
    private String shipCity;
    private String shipStateProvince;
    private String shipPostalCode;
    private String billingAddress1;
    private String billingAddress2;
    private String billingCity;
    private String billingStateProvince;
    private String billingPostalCode;
    private List<SiteContactVO> siteContactList;

    public SiteVO() {

    }

    public SiteVO(String customerAccount, UUID siteId, String siteName, String region, String country, String physicalAddress1, String physicalAddress2, String physicalCity, String physicalStateProvince, String physicalPostalCode, String latitude, String longitude, String timezone, String industry, String shipAddress1, String shipAddress2, String shipCity, String shipStateProvince, String shipPostalCode, String billingAddress1, String billingAddress2, String billingCity, String billingStateProvince, String billingPostalCode, List<SiteContactVO> siteContactList) {
        this.customerAccount = customerAccount;
        this.siteId = siteId;
        this.siteName = siteName;
        this.region = region;
        this.country = country;
        this.physicalAddress1 = physicalAddress1;
        this.physicalAddress2 = physicalAddress2;
        this.physicalCity = physicalCity;
        this.physicalStateProvince = physicalStateProvince;
        this.physicalPostalCode = physicalPostalCode;
        this.latitude = latitude;
        this.longitude = longitude;
        this.timezone = timezone;
        this.industry = industry;
        this.shipAddress1 = shipAddress1;
        this.shipAddress2 = shipAddress2;
        this.shipCity = shipCity;
        this.shipStateProvince = shipStateProvince;
        this.shipPostalCode = shipPostalCode;
        this.billingAddress1 = billingAddress1;
        this.billingAddress2 = billingAddress2;
        this.billingCity = billingCity;
        this.billingStateProvince = billingStateProvince;
        this.billingPostalCode = billingPostalCode;
        this.siteContactList = siteContactList;
    }

    public SiteVO(SiteVO siteVO) {
        customerAccount = siteVO.getCustomerAccount();
        siteId = siteVO.getSiteId();
        siteName = siteVO.getSiteName();
        region = siteVO.getRegion();
        country = siteVO.getCountry();
        physicalAddress1 = siteVO.getPhysicalAddress1();
        physicalAddress2 = siteVO.getPhysicalAddress2();
        physicalCity = siteVO.getPhysicalCity();
        physicalStateProvince = siteVO.getPhysicalStateProvince();
        physicalPostalCode = siteVO.getPhysicalPostalCode();
        latitude = siteVO.getLatitude();
        longitude = siteVO.getLongitude();
        timezone = siteVO.getTimezone();
        industry = siteVO.getIndustry();
        shipAddress1 = siteVO.getShipAddress1();
        shipAddress2 = siteVO.getShipAddress2();
        shipCity = siteVO.getShipCity();
        shipStateProvince = siteVO.getShipStateProvince();
        shipPostalCode = siteVO.getShipPostalCode();
        billingAddress1 = siteVO.getBillingAddress1();
        billingAddress2 = siteVO.getBillingAddress2();
        billingCity = siteVO.getBillingCity();
        billingStateProvince = siteVO.getBillingStateProvince();
        billingPostalCode = siteVO.getBillingPostalCode();
        siteContactList = siteVO.getSiteContactList();

    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPhysicalAddress1() {
        return physicalAddress1;
    }

    public void setPhysicalAddress1(String physicalAddress1) {
        this.physicalAddress1 = physicalAddress1;
    }

    public String getPhysicalAddress2() {
        return physicalAddress2;
    }

    public void setPhysicalAddress2(String physicalAddress2) {
        this.physicalAddress2 = physicalAddress2;
    }

    public String getPhysicalCity() {
        return physicalCity;
    }

    public void setPhysicalCity(String physicalCity) {
        this.physicalCity = physicalCity;
    }

    public String getPhysicalStateProvince() {
        return physicalStateProvince;
    }

    public void setPhysicalStateProvince(String physicalStateProvince) {
        this.physicalStateProvince = physicalStateProvince;
    }

    public String getPhysicalPostalCode() {
        return physicalPostalCode;
    }

    public void setPhysicalPostalCode(String physicalPostalCode) {
        this.physicalPostalCode = physicalPostalCode;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getShipAddress1() {
        return shipAddress1;
    }

    public void setShipAddress1(String shipAddress1) {
        this.shipAddress1 = shipAddress1;
    }

    public String getShipAddress2() {
        return shipAddress2;
    }

    public void setShipAddress2(String shipAddress2) {
        this.shipAddress2 = shipAddress2;
    }

    public String getShipCity() {
        return shipCity;
    }

    public void setShipCity(String shipCity) {
        this.shipCity = shipCity;
    }

    public String getShipStateProvince() {
        return shipStateProvince;
    }

    public void setShipStateProvince(String shipStateProvince) {
        this.shipStateProvince = shipStateProvince;
    }

    public String getShipPostalCode() {
        return shipPostalCode;
    }

    public void setShipPostalCode(String shipPostalCode) {
        this.shipPostalCode = shipPostalCode;
    }

    public String getBillingAddress1() {
        return billingAddress1;
    }

    public void setBillingAddress1(String billingAddress1) {
        this.billingAddress1 = billingAddress1;
    }

    public String getBillingAddress2() {
        return billingAddress2;
    }

    public void setBillingAddress2(String billingAddress2) {
        this.billingAddress2 = billingAddress2;
    }

    public String getBillingCity() {
        return billingCity;
    }

    public void setBillingCity(String billingCity) {
        this.billingCity = billingCity;
    }

    public String getBillingStateProvince() {
        return billingStateProvince;
    }

    public void setBillingStateProvince(String billingStateProvince) {
        this.billingStateProvince = billingStateProvince;
    }

    public String getBillingPostalCode() {
        return billingPostalCode;
    }

    public void setBillingPostalCode(String billingPostalCode) {
        this.billingPostalCode = billingPostalCode;
    }

    public List<SiteContactVO> getSiteContactList() {
        return siteContactList;
    }

    public void setSiteContactList(List<SiteContactVO> siteContactList) {
        this.siteContactList = siteContactList;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.customerAccount);
        hash = 97 * hash + Objects.hashCode(this.siteId);
        hash = 97 * hash + Objects.hashCode(this.siteName);
        hash = 97 * hash + Objects.hashCode(this.region);
        hash = 97 * hash + Objects.hashCode(this.country);
        hash = 97 * hash + Objects.hashCode(this.physicalAddress1);
        hash = 97 * hash + Objects.hashCode(this.physicalAddress2);
        hash = 97 * hash + Objects.hashCode(this.physicalCity);
        hash = 97 * hash + Objects.hashCode(this.physicalStateProvince);
        hash = 97 * hash + Objects.hashCode(this.physicalPostalCode);
        hash = 97 * hash + Objects.hashCode(this.latitude);
        hash = 97 * hash + Objects.hashCode(this.longitude);
        hash = 97 * hash + Objects.hashCode(this.timezone);
        hash = 97 * hash + Objects.hashCode(this.industry);
        hash = 97 * hash + Objects.hashCode(this.shipAddress1);
        hash = 97 * hash + Objects.hashCode(this.shipAddress2);
        hash = 97 * hash + Objects.hashCode(this.shipCity);
        hash = 97 * hash + Objects.hashCode(this.shipStateProvince);
        hash = 97 * hash + Objects.hashCode(this.shipPostalCode);
        hash = 97 * hash + Objects.hashCode(this.billingAddress1);
        hash = 97 * hash + Objects.hashCode(this.billingAddress2);
        hash = 97 * hash + Objects.hashCode(this.billingCity);
        hash = 97 * hash + Objects.hashCode(this.billingStateProvince);
        hash = 97 * hash + Objects.hashCode(this.billingPostalCode);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SiteVO other = (SiteVO) obj;
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.siteName, other.siteName)) {
            return false;
        }
        if (!Objects.equals(this.region, other.region)) {
            return false;
        }
        if (!Objects.equals(this.country, other.country)) {
            return false;
        }
        if (!Objects.equals(this.physicalAddress1, other.physicalAddress1)) {
            return false;
        }
        if (!Objects.equals(this.physicalAddress2, other.physicalAddress2)) {
            return false;
        }
        if (!Objects.equals(this.physicalCity, other.physicalCity)) {
            return false;
        }
        if (!Objects.equals(this.physicalStateProvince, other.physicalStateProvince)) {
            return false;
        }
        if (!Objects.equals(this.physicalPostalCode, other.physicalPostalCode)) {
            return false;
        }
        if (!Objects.equals(this.latitude, other.latitude)) {
            return false;
        }
        if (!Objects.equals(this.longitude, other.longitude)) {
            return false;
        }
        if (!Objects.equals(this.timezone, other.timezone)) {
            return false;
        }
        if (!Objects.equals(this.industry, other.industry)) {
            return false;
        }
        if (!Objects.equals(this.shipAddress1, other.shipAddress1)) {
            return false;
        }
        if (!Objects.equals(this.shipAddress2, other.shipAddress2)) {
            return false;
        }
        if (!Objects.equals(this.shipCity, other.shipCity)) {
            return false;
        }
        if (!Objects.equals(this.shipStateProvince, other.shipStateProvince)) {
            return false;
        }
        if (!Objects.equals(this.shipPostalCode, other.shipPostalCode)) {
            return false;
        }
        if (!Objects.equals(this.billingAddress1, other.billingAddress1)) {
            return false;
        }
        if (!Objects.equals(this.billingAddress2, other.billingAddress2)) {
            return false;
        }
        if (!Objects.equals(this.billingCity, other.billingCity)) {
            return false;
        }
        if (!Objects.equals(this.billingStateProvince, other.billingStateProvince)) {
            return false;
        }
        if (!Objects.equals(this.billingPostalCode, other.billingPostalCode)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "SiteVO{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", siteName=" + siteName + ", region=" + region + ", country=" + country + ", physicalAddress1=" + physicalAddress1 + ", physicalAddress2=" + physicalAddress2 + ", physicalCity=" + physicalCity + ", physicalStateProvince=" + physicalStateProvince + ", physicalPostalCode=" + physicalPostalCode + ", latitude=" + latitude + ", longitude=" + longitude + ", timezone=" + timezone + ", industry=" + industry + ", shipAddress1=" + shipAddress1 + ", shipAddress2=" + shipAddress2 + ", shipCity=" + shipCity + ", shipStateProvince=" + shipStateProvince + ", shipPostalCode=" + shipPostalCode + ", billingAddress1=" + billingAddress1 + ", billingAddress2=" + billingAddress2 + ", billingCity=" + billingCity + ", billingStateProvince=" + billingStateProvince + ", billingPostalCode=" + billingPostalCode + ", siteContactList=" + siteContactList + '}';
    }

}
