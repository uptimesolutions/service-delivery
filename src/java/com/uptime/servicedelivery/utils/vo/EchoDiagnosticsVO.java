/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.servicedelivery.utils.vo;

import java.io.Serializable;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author kpati
 */
public class EchoDiagnosticsVO implements Serializable {
    private String customerAccount;
    private UUID siteId;
    private String siteName;
    private String baseStationHostName;
    private String deviceId;
    private String echoFw;
    private String baseStationMac;
    private short echoDatasets;
    private short echoBacklog;
    private short echoReboot;
    private short echoPowerfault;
    private short echoRestart;
    private byte echoRadioFail;
    private byte echoTerr;
    private byte echoPowLevel;
    private Instant lastUpdate;
    private ZonedDateTime lastUpdateWithSpecfZone;
    private int index;
    
    public EchoDiagnosticsVO(){
    }

    public EchoDiagnosticsVO(String customerAccount, UUID siteId, String siteName, String baseStationHostName, String deviceId, String echoFw, String baseStationMac, short echoDatasets, short echoBacklog, short echoReboot, short echoPowerfault, short echoRestart, byte echoRadioFail, byte echoTerr, byte echoPowLevel, Instant lastUpdate, ZonedDateTime lastUpdateWithSpecfZone, int index) {
        this.customerAccount = customerAccount;
        this.siteId = siteId;
        this.siteName = siteName;
        this.baseStationHostName = baseStationHostName;
        this.deviceId = deviceId;
        this.echoFw = echoFw;
        this.baseStationMac = baseStationMac;
        this.echoDatasets = echoDatasets;
        this.echoBacklog = echoBacklog;
        this.echoReboot = echoReboot;
        this.echoPowerfault = echoPowerfault;
        this.echoRestart = echoRestart;
        this.echoRadioFail = echoRadioFail;
        this.echoTerr = echoTerr;
        this.echoPowLevel = echoPowLevel;
        this.lastUpdate = lastUpdate;
        this.lastUpdateWithSpecfZone = lastUpdateWithSpecfZone;
        this.index = index;
    }
    
    public EchoDiagnosticsVO(EchoDiagnosticsVO echoDiagnosticsVO){
        customerAccount = echoDiagnosticsVO.getCustomerAccount();
        siteId = echoDiagnosticsVO.getSiteId();
        siteName = echoDiagnosticsVO.getSiteName();
        baseStationHostName = echoDiagnosticsVO.getBaseStationHostName();
        deviceId = echoDiagnosticsVO.getDeviceId();
        echoFw = echoDiagnosticsVO.getEchoFw();
        baseStationMac = echoDiagnosticsVO.getBaseStationMac();
        echoDatasets = echoDiagnosticsVO.getEchoDatasets();
        echoBacklog = echoDiagnosticsVO.getEchoBacklog();
        echoReboot = echoDiagnosticsVO.getEchoReboot();
        echoPowerfault = echoDiagnosticsVO.getEchoPowerfault();
        echoRestart = echoDiagnosticsVO.getEchoRestart();
        echoRadioFail = echoDiagnosticsVO.getEchoRadioFail();
        echoTerr = echoDiagnosticsVO.getEchoTerr();
        echoPowLevel = echoDiagnosticsVO.getEchoPowLevel();
        lastUpdate = echoDiagnosticsVO.getLastUpdate();
        lastUpdateWithSpecfZone = echoDiagnosticsVO.getLastUpdateWithSpecfZone();
        index = echoDiagnosticsVO.getIndex();
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getBaseStationHostName() {
        return baseStationHostName;
    }

    public void setBaseStationHostName(String baseStationHostName) {
        this.baseStationHostName = baseStationHostName;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getEchoFw() {
        return echoFw;
    }

    public void setEchoFw(String echoFw) {
        this.echoFw = echoFw;
    }

    public String getBaseStationMac() {
        return baseStationMac;
    }

    public void setBaseStationMac(String baseStationMac) {
        this.baseStationMac = baseStationMac;
    }

    public short getEchoDatasets() {
        return echoDatasets;
    }

    public void setEchoDatasets(short echoDatasets) {
        this.echoDatasets = echoDatasets;
    }

    public short getEchoBacklog() {
        return echoBacklog;
    }

    public void setEchoBacklog(short echoBacklog) {
        this.echoBacklog = echoBacklog;
    }

    public short getEchoReboot() {
        return echoReboot;
    }

    public void setEchoReboot(short echoReboot) {
        this.echoReboot = echoReboot;
    }

    public short getEchoPowerfault() {
        return echoPowerfault;
    }

    public void setEchoPowerfault(short echoPowerfault) {
        this.echoPowerfault = echoPowerfault;
    }

    public short getEchoRestart() {
        return echoRestart;
    }

    public void setEchoRestart(short echoRestart) {
        this.echoRestart = echoRestart;
    }

    public byte getEchoRadioFail() {
        return echoRadioFail;
    }

    public void setEchoRadioFail(byte echoRadioFail) {
        this.echoRadioFail = echoRadioFail;
    }

    public byte getEchoTerr() {
        return echoTerr;
    }

    public void setEchoTerr(byte echoTerr) {
        this.echoTerr = echoTerr;
    }

    public byte getEchoPowLevel() {
        return echoPowLevel;
    }

    public void setEchoPowLevel(byte echoPowLevel) {
        this.echoPowLevel = echoPowLevel;
    }

    public Instant getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Instant lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public ZonedDateTime getLastUpdateWithSpecfZone() {
        return lastUpdateWithSpecfZone;
    }

    public void setLastUpdateWithSpecfZone(ZonedDateTime lastUpdateWithSpecfZone) {
        this.lastUpdateWithSpecfZone = lastUpdateWithSpecfZone;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getFormatedLastUpdate() {
        if (lastUpdateWithSpecfZone != null) {
            String timeStamp [] = lastUpdateWithSpecfZone.toString().split("T");
            String minSec[]= timeStamp[1].split("-");
            return timeStamp[0]+" "+minSec[0];
        }
        return "";
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.customerAccount);
        hash = 23 * hash + Objects.hashCode(this.siteId);
        hash = 23 * hash + Objects.hashCode(this.siteName);
        hash = 23 * hash + Objects.hashCode(this.baseStationHostName);
        hash = 23 * hash + Objects.hashCode(this.deviceId);
        hash = 23 * hash + Objects.hashCode(this.echoFw);
        hash = 23 * hash + Objects.hashCode(this.baseStationMac);
        hash = 23 * hash + this.echoDatasets;
        hash = 23 * hash + this.echoBacklog;
        hash = 23 * hash + this.echoReboot;
        hash = 23 * hash + this.echoPowerfault;
        hash = 23 * hash + this.echoRestart;
        hash = 23 * hash + this.echoRadioFail;
        hash = 23 * hash + this.echoTerr;
        hash = 23 * hash + this.echoPowLevel;
        hash = 23 * hash + Objects.hashCode(this.lastUpdate);
        hash = 23 * hash + Objects.hashCode(this.lastUpdateWithSpecfZone);
        hash = 23 * hash + this.index;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EchoDiagnosticsVO other = (EchoDiagnosticsVO) obj;
        if (this.echoDatasets != other.echoDatasets) {
            return false;
        }
        if (this.echoBacklog != other.echoBacklog) {
            return false;
        }
        if (this.echoReboot != other.echoReboot) {
            return false;
        }
        if (this.echoPowerfault != other.echoPowerfault) {
            return false;
        }
        if (this.echoRestart != other.echoRestart) {
            return false;
        }
        if (this.echoRadioFail != other.echoRadioFail) {
            return false;
        }
        if (this.echoTerr != other.echoTerr) {
            return false;
        }
        if (this.echoPowLevel != other.echoPowLevel) {
            return false;
        }
        if (this.index != other.index) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.siteName, other.siteName)) {
            return false;
        }
        if (!Objects.equals(this.baseStationHostName, other.baseStationHostName)) {
            return false;
        }
        if (!Objects.equals(this.deviceId, other.deviceId)) {
            return false;
        }
        if (!Objects.equals(this.echoFw, other.echoFw)) {
            return false;
        }
        if (!Objects.equals(this.baseStationMac, other.baseStationMac)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.lastUpdate, other.lastUpdate)) {
            return false;
        }
        if (!Objects.equals(this.lastUpdateWithSpecfZone, other.lastUpdateWithSpecfZone)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "EchoDiagnosticsVO{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", siteName=" + siteName + ", baseStationHostName=" + baseStationHostName + ", deviceId=" + deviceId + ", echoFw=" + echoFw + ", baseStationMac=" + baseStationMac + ", echoDatasets=" + echoDatasets + ", echoBacklog=" + echoBacklog + ", echoReboot=" + echoReboot + ", echoPowerfault=" + echoPowerfault + ", echoRestart=" + echoRestart + ", echoRadioFail=" + echoRadioFail + ", echoTerr=" + echoTerr + ", echoPowLevel=" + echoPowLevel + ", lastUpdate=" + lastUpdate + ", lastUpdateWithSpecfZone=" + lastUpdateWithSpecfZone + ", index=" + index + '}';
    }
}
