/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.servicedelivery.utils.vo;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author twilcox
 */
public class CustomersVO implements Serializable {
    private String customerAccount;
    private String customerName;
    private String fqdn;
    private String logo;
    private String skin;
    private String sloUrl;

    public CustomersVO() {
    }

    public CustomersVO(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public CustomersVO(String customerAccount, String customerName) {
        this.customerAccount = customerAccount;
        this.customerName = customerName;
    }

    public CustomersVO(String customerAccount, String customerName, String fqdn) {
        this.customerAccount = customerAccount;
        this.customerName = customerName;
        this.fqdn = fqdn;
    }

    public CustomersVO(CustomersVO customersVO) {
        customerAccount = customersVO.getCustomerAccount();
        customerName = customersVO.getCustomerName();
        fqdn = customersVO.getFqdn();
        logo = customersVO.getLogo();
        skin = customersVO.getSkin();
        sloUrl = customersVO.getSloUrl();
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getFqdn() {
        return fqdn;
    }

    public void setFqdn(String fqdn) {
        this.fqdn = fqdn;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getSkin() {
        return skin;
    }

    public void setSkin(String skin) {
        this.skin = skin;
    }

    public String getSloUrl() {
        return sloUrl;
    }

    public void setSloUrl(String sloUrl) {
        this.sloUrl = sloUrl;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + Objects.hashCode(this.customerAccount);
        hash = 41 * hash + Objects.hashCode(this.customerName);
        hash = 41 * hash + Objects.hashCode(this.fqdn);
        hash = 41 * hash + Objects.hashCode(this.logo);
        hash = 41 * hash + Objects.hashCode(this.skin);
        hash = 41 * hash + Objects.hashCode(this.sloUrl);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CustomersVO other = (CustomersVO) obj;
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.customerName, other.customerName)) {
            return false;
        }
        if (!Objects.equals(this.fqdn, other.fqdn)) {
            return false;
        }
        if (!Objects.equals(this.logo, other.logo)) {
            return false;
        }
        if (!Objects.equals(this.skin, other.skin)) {
            return false;
        }
        if (!Objects.equals(this.sloUrl, other.sloUrl)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "CustomersVO{" + "customerAccount=" + customerAccount + ", customerName=" + customerName + ", fqdn=" + fqdn + ", logo=" + logo + ", skin=" + skin + ", sloUrl=" + sloUrl + '}';
    }
}
