/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.servicedelivery.utils.vo;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author kpati
 */
public class SiteContactVO implements Serializable {
    private String customerAccount;
    private String siteId;
    private String siteName;
    private String role;
    private String contactName;
    private String contactTitle;
    private String contactPhone;
    private String contactEmail;
    private boolean roleReadOnly;

    public SiteContactVO() {
    }

    public SiteContactVO(String customerAccount, String siteId, String siteName, String role, String contactName, String contactTitle, String contactPhone, String contactEmail, boolean roleReadOnly) {
        this.customerAccount = customerAccount;
        this.siteId = siteId;
        this.siteName = siteName;
        this.role = role;
        this.contactName = contactName;
        this.contactTitle = contactTitle;
        this.contactPhone = contactPhone;
        this.contactEmail = contactEmail;
        this.roleReadOnly = roleReadOnly;
    }

    public SiteContactVO(SiteContactVO siteContactVO) {
        customerAccount = siteContactVO.getCustomerAccount();
        siteId = siteContactVO.getSiteId();
        siteName = siteContactVO.getSiteName();
        role = siteContactVO.getRole();
        contactName = siteContactVO.getContactName();
        contactTitle = siteContactVO.getContactTitle();
        contactPhone = siteContactVO.getContactPhone();
        contactEmail = siteContactVO.getContactEmail();
        roleReadOnly = siteContactVO.isRoleReadOnly();
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public String getSiteId() {
        return siteId;
    }

    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactTitle() {
        return contactTitle;
    }

    public void setContactTitle(String contactTitle) {
        this.contactTitle = contactTitle;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public boolean isRoleReadOnly() {
        return roleReadOnly;
    }

    public void setRoleReadOnly(boolean roleReadOnly) {
        this.roleReadOnly = roleReadOnly;
    }

    
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.customerAccount);
        hash = 97 * hash + Objects.hashCode(this.siteName);
        hash = 97 * hash + Objects.hashCode(this.role);
        hash = 97 * hash + Objects.hashCode(this.contactName);
        hash = 97 * hash + Objects.hashCode(this.contactTitle);
        hash = 97 * hash + Objects.hashCode(this.contactPhone);
        hash = 97 * hash + Objects.hashCode(this.contactEmail);

        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SiteContactVO other = (SiteContactVO) obj;
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.siteName, other.siteName)) {
            return false;
        }
        if (!Objects.equals(this.role, other.role)) {
            return false;
        }
        if (!Objects.equals(this.contactName, other.contactName)) {
            return false;
        }
        if (!Objects.equals(this.contactTitle, other.contactTitle)) {
            return false;
        }
        if (!Objects.equals(this.contactPhone, other.contactPhone)) {
            return false;
        }
        if (!Objects.equals(this.contactEmail, other.contactEmail)) {
            return false;
        }

        return true;
    }

    @Override
    public String toString() {
        return "SiteContactVO{" + "customerAccount=" + customerAccount + ", siteName=" + siteName + ", role=" + role + ", contactName=" + contactName + ", contactTitle=" + contactTitle + ", contactPhone=" + contactPhone + ", contactEmail=" + contactEmail + '}';
    }

}
