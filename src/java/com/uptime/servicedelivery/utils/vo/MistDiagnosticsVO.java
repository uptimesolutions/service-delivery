/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.servicedelivery.utils.vo;

import java.io.Serializable;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author kpati
 */
public class MistDiagnosticsVO implements Serializable {
    private String customerAccount;
    private UUID siteId;
    private String echoDeviceId;
    private String deviceId;
    private byte mistlxChan;
    private Instant lastSample;
    private byte mistlxRssi;
    private byte mistlxFsamp;
    private short mistlxLength;
    private float mistlxBattery;
    private float mistlxTemp;
    private short mistlxDelta;
    private short mistlxPcorr;
    private byte mistlxDbg;
    private byte mistlxBleon;
    private byte mistlxFpgaon;
    private short mistlxTotalMin;
    private short mistlxMincnt;
    private short mistlxFailcnt;
    private short mistlxCtime;
    private short mistlxCtimeMod;
    private String siteName;
    private String areaName;
    private String assetName;
    private String pointLocationName;
    private String pointName;
    private short baseStationPort;
    private String baseStationMac;
    private String baseStationHostName;
    private ZonedDateTime lastSampleWithSpecfZone;
    private int index;
    
    public MistDiagnosticsVO(){
    }

    public MistDiagnosticsVO(String customerAccount, UUID siteId, String echoDeviceId, String deviceId, byte mistlxChan, Instant lastSample, byte mistlxRssi, byte mistlxFsamp, short mistlxLength, float mistlxBattery, float mistlxTemp, short mistlxDelta, short mistlxPcorr, byte mistlxDbg, byte mistlxBleon, byte mistlxFpgaon, short mistlxTotalMin, short mistlxMincnt, short mistlxFailcnt, short mistlxCtime, short mistlxCtimeMod, String siteName, String areaName, String assetName, String pointLocationName, String pointName, short baseStationPort, String baseStationMac, String baseStationHostName, ZonedDateTime lastSampleWithSpecfZone, int index) {
        this.customerAccount = customerAccount;
        this.siteId = siteId;
        this.echoDeviceId = echoDeviceId;
        this.deviceId = deviceId;
        this.mistlxChan = mistlxChan;
        this.lastSample = lastSample;
        this.mistlxRssi = mistlxRssi;
        this.mistlxFsamp = mistlxFsamp;
        this.mistlxLength = mistlxLength;
        this.mistlxBattery = mistlxBattery;
        this.mistlxTemp = mistlxTemp;
        this.mistlxDelta = mistlxDelta;
        this.mistlxPcorr = mistlxPcorr;
        this.mistlxDbg = mistlxDbg;
        this.mistlxBleon = mistlxBleon;
        this.mistlxFpgaon = mistlxFpgaon;
        this.mistlxTotalMin = mistlxTotalMin;
        this.mistlxMincnt = mistlxMincnt;
        this.mistlxFailcnt = mistlxFailcnt;
        this.mistlxCtime = mistlxCtime;
        this.mistlxCtimeMod = mistlxCtimeMod;
        this.siteName = siteName;
        this.areaName = areaName;
        this.assetName = assetName;
        this.pointLocationName = pointLocationName;
        this.pointName = pointName;
        this.baseStationPort = baseStationPort;
        this.baseStationMac = baseStationMac;
        this.baseStationHostName = baseStationHostName;
        this.lastSampleWithSpecfZone = lastSampleWithSpecfZone;
        this.index = index;
    }

    public MistDiagnosticsVO(MistDiagnosticsVO mistDiagnosticsVO) {
        customerAccount = mistDiagnosticsVO.getCustomerAccount();
        siteId = mistDiagnosticsVO.getSiteId();
        echoDeviceId = mistDiagnosticsVO.getEchoDeviceId();
        deviceId = mistDiagnosticsVO.getDeviceId();
        mistlxChan = mistDiagnosticsVO.getMistlxChan();
        lastSample = mistDiagnosticsVO.getLastSample();
        mistlxRssi = mistDiagnosticsVO.getMistlxRssi();
        mistlxFsamp = mistDiagnosticsVO.getMistlxFsamp();
        mistlxLength = mistDiagnosticsVO.getMistlxLength();
        mistlxBattery = mistDiagnosticsVO.getMistlxBattery();
        mistlxTemp = mistDiagnosticsVO.getMistlxTemp();
        mistlxDelta = mistDiagnosticsVO.getMistlxDelta();
        mistlxPcorr = mistDiagnosticsVO.getMistlxPcorr();
        mistlxDbg = mistDiagnosticsVO.getMistlxDbg();
        mistlxBleon = mistDiagnosticsVO.getMistlxBleon();
        mistlxFpgaon = mistDiagnosticsVO.getMistlxFpgaon();
        mistlxTotalMin = mistDiagnosticsVO.getMistlxTotalMin();
        mistlxMincnt = mistDiagnosticsVO.getMistlxMincnt();
        mistlxFailcnt = mistDiagnosticsVO.getMistlxFailcnt();
        mistlxCtime = mistDiagnosticsVO.getMistlxCtime();
        mistlxCtimeMod = mistDiagnosticsVO.getMistlxCtimeMod();
        siteName = mistDiagnosticsVO.getSiteName();
        areaName = mistDiagnosticsVO.getAreaName();
        assetName = mistDiagnosticsVO.getAssetName();
        pointLocationName = mistDiagnosticsVO.getPointLocationName();
        pointName = mistDiagnosticsVO.getPointName();
        baseStationPort = mistDiagnosticsVO.getBaseStationPort();
        baseStationMac = mistDiagnosticsVO.getBaseStationMac();
        baseStationHostName = mistDiagnosticsVO.getBaseStationHostName();
        lastSampleWithSpecfZone = mistDiagnosticsVO.getLastSampleWithSpecfZone();
        index = mistDiagnosticsVO.getIndex();
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public String getEchoDeviceId() {
        return echoDeviceId;
    }

    public void setEchoDeviceId(String echoDeviceId) {
        this.echoDeviceId = echoDeviceId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public byte getMistlxChan() {
        return mistlxChan;
    }

    public void setMistlxChan(byte mistlxChan) {
        this.mistlxChan = mistlxChan;
    }

    public Instant getLastSample() {
        return lastSample;
    }

    public void setLastSample(Instant lastSample) {
        this.lastSample = lastSample;
    }

    public byte getMistlxRssi() {
        return mistlxRssi;
    }

    public void setMistlxRssi(byte mistlxRssi) {
        this.mistlxRssi = mistlxRssi;
    }

    public byte getMistlxFsamp() {
        return mistlxFsamp;
    }

    public void setMistlxFsamp(byte mistlxFsamp) {
        this.mistlxFsamp = mistlxFsamp;
    }

    public short getMistlxLength() {
        return mistlxLength;
    }

    public void setMistlxLength(short mistlxLength) {
        this.mistlxLength = mistlxLength;
    }

    public float getMistlxBattery() {
        return mistlxBattery;
    }

    public void setMistlxBattery(float mistlxBattery) {
        this.mistlxBattery = mistlxBattery;
    }

    public float getMistlxTemp() {
        return mistlxTemp;
    }

    public void setMistlxTemp(float mistlxTemp) {
        this.mistlxTemp = mistlxTemp;
    }

    public short getMistlxDelta() {
        return mistlxDelta;
    }

    public void setMistlxDelta(short mistlxDelta) {
        this.mistlxDelta = mistlxDelta;
    }

    public short getMistlxPcorr() {
        return mistlxPcorr;
    }

    public void setMistlxPcorr(short mistlxPcorr) {
        this.mistlxPcorr = mistlxPcorr;
    }

    public byte getMistlxDbg() {
        return mistlxDbg;
    }

    public void setMistlxDbg(byte mistlxDbg) {
        this.mistlxDbg = mistlxDbg;
    }

    public byte getMistlxBleon() {
        return mistlxBleon;
    }

    public void setMistlxBleon(byte mistlxBleon) {
        this.mistlxBleon = mistlxBleon;
    }

    public byte getMistlxFpgaon() {
        return mistlxFpgaon;
    }

    public void setMistlxFpgaon(byte mistlxFpgaon) {
        this.mistlxFpgaon = mistlxFpgaon;
    }

    public short getMistlxTotalMin() {
        return mistlxTotalMin;
    }

    public void setMistlxTotalMin(short mistlxTotalMin) {
        this.mistlxTotalMin = mistlxTotalMin;
    }

    public short getMistlxMincnt() {
        return mistlxMincnt;
    }

    public void setMistlxMincnt(short mistlxMincnt) {
        this.mistlxMincnt = mistlxMincnt;
    }

    public short getMistlxFailcnt() {
        return mistlxFailcnt;
    }

    public void setMistlxFailcnt(short mistlxFailcnt) {
        this.mistlxFailcnt = mistlxFailcnt;
    }

    public short getMistlxCtime() {
        return mistlxCtime;
    }

    public void setMistlxCtime(short mistlxCtime) {
        this.mistlxCtime = mistlxCtime;
    }

    public short getMistlxCtimeMod() {
        return mistlxCtimeMod;
    }

    public void setMistlxCtimeMod(short mistlxCtimeMod) {
        this.mistlxCtimeMod = mistlxCtimeMod;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }

    public String getPointLocationName() {
        return pointLocationName;
    }

    public void setPointLocationName(String pointLocationName) {
        this.pointLocationName = pointLocationName;
    }

    public String getPointName() {
        return pointName;
    }

    public void setPointName(String pointName) {
        this.pointName = pointName;
    }

    public short getBaseStationPort() {
        return baseStationPort;
    }

    public void setBaseStationPort(short baseStationPort) {
        this.baseStationPort = baseStationPort;
    }

    public String getBaseStationMac() {
        return baseStationMac;
    }

    public void setBaseStationMac(String baseStationMac) {
        this.baseStationMac = baseStationMac;
    }

    public String getBaseStationHostName() {
        return baseStationHostName;
    }

    public void setBaseStationHostName(String baseStationHostName) {
        this.baseStationHostName = baseStationHostName;
    }

    public ZonedDateTime getLastSampleWithSpecfZone() {
        return lastSampleWithSpecfZone;
    }

    public void setLastSampleWithSpecfZone(ZonedDateTime lastSampleWithSpecfZone) {
        this.lastSampleWithSpecfZone = lastSampleWithSpecfZone;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getFormatedLastSample() {
        if (lastSampleWithSpecfZone != null) {
            String timeStamp [] = lastSampleWithSpecfZone.toString().split("T");
            String minSec[]= timeStamp[1].split("-");
            return timeStamp[0]+" "+minSec[0];
        }
        return "";
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.customerAccount);
        hash = 97 * hash + Objects.hashCode(this.siteId);
        hash = 97 * hash + Objects.hashCode(this.echoDeviceId);
        hash = 97 * hash + Objects.hashCode(this.deviceId);
        hash = 97 * hash + this.mistlxChan;
        hash = 97 * hash + Objects.hashCode(this.lastSample);
        hash = 97 * hash + this.mistlxRssi;
        hash = 97 * hash + this.mistlxFsamp;
        hash = 97 * hash + this.mistlxLength;
        hash = 97 * hash + Float.floatToIntBits(this.mistlxBattery);
        hash = 97 * hash + Float.floatToIntBits(this.mistlxTemp);
        hash = 97 * hash + this.mistlxDelta;
        hash = 97 * hash + this.mistlxPcorr;
        hash = 97 * hash + this.mistlxDbg;
        hash = 97 * hash + this.mistlxBleon;
        hash = 97 * hash + this.mistlxFpgaon;
        hash = 97 * hash + this.mistlxTotalMin;
        hash = 97 * hash + this.mistlxMincnt;
        hash = 97 * hash + this.mistlxFailcnt;
        hash = 97 * hash + this.mistlxCtime;
        hash = 97 * hash + this.mistlxCtimeMod;
        hash = 97 * hash + Objects.hashCode(this.siteName);
        hash = 97 * hash + Objects.hashCode(this.areaName);
        hash = 97 * hash + Objects.hashCode(this.assetName);
        hash = 97 * hash + Objects.hashCode(this.pointLocationName);
        hash = 97 * hash + Objects.hashCode(this.pointName);
        hash = 97 * hash + this.baseStationPort;
        hash = 97 * hash + Objects.hashCode(this.baseStationMac);
        hash = 97 * hash + Objects.hashCode(this.baseStationHostName);
        hash = 97 * hash + Objects.hashCode(this.lastSampleWithSpecfZone);
        hash = 97 * hash + this.index;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MistDiagnosticsVO other = (MistDiagnosticsVO) obj;
        if (this.mistlxChan != other.mistlxChan) {
            return false;
        }
        if (this.mistlxRssi != other.mistlxRssi) {
            return false;
        }
        if (this.mistlxFsamp != other.mistlxFsamp) {
            return false;
        }
        if (this.mistlxLength != other.mistlxLength) {
            return false;
        }
        if (Float.floatToIntBits(this.mistlxBattery) != Float.floatToIntBits(other.mistlxBattery)) {
            return false;
        }
        if (Float.floatToIntBits(this.mistlxTemp) != Float.floatToIntBits(other.mistlxTemp)) {
            return false;
        }
        if (this.mistlxDelta != other.mistlxDelta) {
            return false;
        }
        if (this.mistlxPcorr != other.mistlxPcorr) {
            return false;
        }
        if (this.mistlxDbg != other.mistlxDbg) {
            return false;
        }
        if (this.mistlxBleon != other.mistlxBleon) {
            return false;
        }
        if (this.mistlxFpgaon != other.mistlxFpgaon) {
            return false;
        }
        if (this.mistlxTotalMin != other.mistlxTotalMin) {
            return false;
        }
        if (this.mistlxMincnt != other.mistlxMincnt) {
            return false;
        }
        if (this.mistlxFailcnt != other.mistlxFailcnt) {
            return false;
        }
        if (this.mistlxCtime != other.mistlxCtime) {
            return false;
        }
        if (this.mistlxCtimeMod != other.mistlxCtimeMod) {
            return false;
        }
        if (this.baseStationPort != other.baseStationPort) {
            return false;
        }
        if (this.index != other.index) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.echoDeviceId, other.echoDeviceId)) {
            return false;
        }
        if (!Objects.equals(this.deviceId, other.deviceId)) {
            return false;
        }
        if (!Objects.equals(this.siteName, other.siteName)) {
            return false;
        }
        if (!Objects.equals(this.areaName, other.areaName)) {
            return false;
        }
        if (!Objects.equals(this.assetName, other.assetName)) {
            return false;
        }
        if (!Objects.equals(this.pointLocationName, other.pointLocationName)) {
            return false;
        }
        if (!Objects.equals(this.pointName, other.pointName)) {
            return false;
        }
        if (!Objects.equals(this.baseStationMac, other.baseStationMac)) {
            return false;
        }
        if (!Objects.equals(this.baseStationHostName, other.baseStationHostName)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.lastSample, other.lastSample)) {
            return false;
        }
        if (!Objects.equals(this.lastSampleWithSpecfZone, other.lastSampleWithSpecfZone)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "MistDiagnosticsVO{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", echoDeviceId=" + echoDeviceId + ", deviceId=" + deviceId + ", mistlxChan=" + mistlxChan + ", lastSample=" + lastSample + ", mistlxRssi=" + mistlxRssi + ", mistlxFsamp=" + mistlxFsamp + ", mistlxLength=" + mistlxLength + ", mistlxBattery=" + mistlxBattery + ", mistlxTemp=" + mistlxTemp + ", mistlxDelta=" + mistlxDelta + ", mistlxPcorr=" + mistlxPcorr + ", mistlxDbg=" + mistlxDbg + ", mistlxBleon=" + mistlxBleon + ", mistlxFpgaon=" + mistlxFpgaon + ", mistlxTotalMin=" + mistlxTotalMin + ", mistlxMincnt=" + mistlxMincnt + ", mistlxFailcnt=" + mistlxFailcnt + ", mistlxCtime=" + mistlxCtime + ", mistlxCtimeMod=" + mistlxCtimeMod + ", siteName=" + siteName + ", areaName=" + areaName + ", assetName=" + assetName + ", pointLocationName=" + pointLocationName + ", pointName=" + pointName + ", baseStationPort=" + baseStationPort + ", baseStationMac=" + baseStationMac + ", baseStationHostName=" + baseStationHostName + ", lastSampleWithSpecfZone=" + lastSampleWithSpecfZone + ", index=" + index + '}';
    }
}