/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.servicedelivery.utils.converter;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.SelectItem;

/**
 *
 * @author twilcox
 */
@FacesConverter("selectItemConverter")
public class SelectItemConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
        SelectItem selectItem = null; 
        JsonElement element;
        JsonObject jsonObject;
        
        if(value != null && !value.isEmpty()) {
            if(value.equals("Not SelectItem")) {
                return null;
            }
            try {
                if ((element = JsonParser.parseString(value)) != null && (jsonObject = element.getAsJsonObject()) != null) {
                    if (jsonObject.has("label")) {
                        selectItem = new SelectItem();
                        selectItem.setLabel(jsonObject.get("label").getAsString());
                    }
                    if (jsonObject.has("uuidValue")) {
                        if (selectItem == null) {
                            selectItem = new SelectItem();
                        }
                        selectItem.setValue(UUID.fromString(jsonObject.get("uuidValue").getAsString()));
                    }
                    if (jsonObject.has("description")) {
                        if (selectItem == null) {
                            selectItem = new SelectItem();
                        }
                        selectItem.setDescription(jsonObject.get("description").getAsString());
                    }
                }
            } catch (Exception e) {
                Logger.getLogger(SelectItemConverter.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                selectItem = null;
            }
        }
        return selectItem;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        SelectItem selectItem;
        StringBuilder sb;
        
        if (object != null) {
            if (object instanceof SelectItem) {
                selectItem = (SelectItem)object;
                if (selectItem.getValue() != null) {
                    if (selectItem.getValue() instanceof UUID) {
                        sb = new StringBuilder();
                        sb.append("{");
                        if (selectItem.getLabel() != null) {
                            sb.append("\"label\":\"").append(selectItem.getLabel()).append("\",");
                        }
                        if (selectItem.getDescription() != null) {
                            sb.append("\"description\":\"").append(selectItem.getDescription()).append("\",");
                        }
                        sb.append("\"uuidValue\":\"").append(((UUID)selectItem.getValue()).toString()).append("\"}");
                        return sb.toString();
                    }
                } else if (selectItem.getLabel() != null && selectItem.getValue() == null) {
                    sb = new StringBuilder();
                        sb.append("{");
                        if (selectItem.getDescription() != null) {
                            sb.append("\"description\":\"").append(selectItem.getDescription()).append("\",");
                        }
                    sb.append("\"label\":\"").append(selectItem.getLabel()).append("\"}");
                    return sb.toString();
                }
            } else {
                Logger.getLogger(SelectItemConverter.class.getName()).log(Level.SEVERE, "Error: Unknown Object type sent.");
            }
        }
        return "Not SelectItem";
    }
}
