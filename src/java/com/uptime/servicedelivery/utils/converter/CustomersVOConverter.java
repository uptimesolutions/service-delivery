/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.servicedelivery.utils.converter;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.uptime.client.utils.JsonConverterUtil;
import com.uptime.servicedelivery.utils.vo.CustomersVO;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author twilcox
 */
@FacesConverter("customersVOConverter")
public class CustomersVOConverter implements Converter {
    
    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
        CustomersVO customerAccountVO = null; 
        JsonElement element;
        JsonObject jsonObject;
        
        if(value != null && !value.isEmpty()) {
            if(value.equals("Not CustomersVO")) {
                return null;
            }
            try {
                if ((element = JsonParser.parseString(value)) != null && (jsonObject = element.getAsJsonObject()) != null) {
                    customerAccountVO = new CustomersVO();
                    if (jsonObject.has("customerName")) {
                        try {
                            customerAccountVO.setCustomerName(jsonObject.get("customerName").getAsString());
                        }catch (Exception ex) {}
                    }
                    if (jsonObject.has("customerAccount")) {
                        try {
                            customerAccountVO.setCustomerAccount(jsonObject.get("customerAccount").getAsString());
                        }catch (Exception ex) {}
                    }
                    if (jsonObject.has("fqdn")) {
                        try {
                            customerAccountVO.setFqdn(jsonObject.get("fqdn").getAsString());
                        }catch (Exception ex) {}
                    }
                }
            } catch (Exception e) {
                Logger.getLogger(CustomersVOConverter.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                customerAccountVO = null;
            }
        }
        return customerAccountVO;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if (object != null) {
            if (object instanceof CustomersVO) {
                try {
                    return JsonConverterUtil.toJSON((CustomersVO)object);
                } catch (Exception e) {
                    Logger.getLogger(CustomersVOConverter.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                }
            } else {
                Logger.getLogger(CustomersVOConverter.class.getName()).log(Level.SEVERE, "Error: Unknown Object type sent.");
            }
        }
        return "Not CustomersVO";
    }
    
}
