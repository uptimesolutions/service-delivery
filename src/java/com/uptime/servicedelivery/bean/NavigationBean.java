/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.servicedelivery.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.swing.tree.TreeNode;
import org.primefaces.PrimeFaces;
import org.primefaces.event.NodeCollapseEvent;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.MenuModel;

/**
 *
 * @author twilcox
 */
public class NavigationBean implements Serializable {
    private String menuMode, page, dialogCard, dialogHeader, dialogContent, childDialogContent;
    private TreeNode treeNodeRoot, selectedNode;
    private transient MenuModel breadcrumbModel;
    private boolean rightPanelButton, topbarControlAddButton, leftTreePanelButton;

    /**
     * Constructor
     */
    public NavigationBean() {
        menuMode = "layout-static";
        page = "home";
    }

    /**
     * Post Constructor
     */
    @PostConstruct
    public void init() {
        updateDialog(null);
        updatePage("home");
    }
    
    /**
     * Change the menuMode between "layout-slim" and "layout-static"
     */
    public void changeMenuMode() {
        PrimeFaces pf = PrimeFaces.current();
        
        if (menuMode.equalsIgnoreCase("layout-slim")) {
            menuMode = "layout-static layout-static-active";
            pf.executeScript("PrimeFaces.RomaConfigurator.changeMenuMode('layout-static layout-static-active');");
        } else {
            menuMode = "layout-slim";
            pf.executeScript("PrimeFaces.RomaConfigurator.changeMenuMode('layout-slim');");
        }
        pf.ajax().update("menuFormId");
    }

    /**
     * Update page based on the given page
     * @param page, String Object
     * @return String object
     */
    public String updatePage(String page) {
        DefaultMenuItem index = new DefaultMenuItem();
        
        try {
            breadcrumbModel = new DefaultMenuModel();
            if(page != null) {
                switch(this.page = page) {
                    case"home":
                        index.setValue("Home");
                        index.setIcon("pi pi-fw pi-home");
                        breadcrumbModel.getElements().add(index);
                        rightPanelButton = false;
                        leftTreePanelButton = false;
                        topbarControlAddButton = false;
                        return this.page;
                    case"echobases":
                        index.setValue("Monitoring");
                        index.setIcon("pi pi-fw pi-desktop");
                        breadcrumbModel.getElements().add(index);
                        
                        index = new DefaultMenuItem();
                        index.setValue("Echobases");
                        index.setIcon("fa fa-bullseye");
                        breadcrumbModel.getElements().add(index);
                        
                        rightPanelButton = false;
                        leftTreePanelButton = false;
                        topbarControlAddButton = true;

                        if ((EchobasesBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("echobasesBean") == null) {
                            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("echobasesBean", new EchobasesBean());
                            ((EchobasesBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("echobasesBean")).init();
                        } else {
                            ((EchobasesBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("echobasesBean")).init();
                        }
                        
                        return this.page;
                    case"devices":
                        index.setValue("Monitoring");
                        index.setIcon("pi pi-fw pi-desktop");
                        breadcrumbModel.getElements().add(index);
                        
                        index = new DefaultMenuItem();
                        index.setValue("Devices");
                        index.setIcon("fa fa-rss");
                        breadcrumbModel.getElements().add(index);
                        
                        rightPanelButton = false;
                        leftTreePanelButton = false;
                        topbarControlAddButton = true;

                        if ((DevicesBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("devicesBean") == null) {
                            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("devicesBean", new DevicesBean());
                            ((DevicesBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("devicesBean")).init();
                        } else {
                            ((DevicesBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("devicesBean")).init();
                        }
                        
                        return this.page;
                    case"maintenance":
                        index.setValue("Maintenance");
                        index.setIcon("pi pi-fw pi-sliders-v");
                        breadcrumbModel.getElements().add(index);
                        rightPanelButton = true;
                        leftTreePanelButton = true;
                        topbarControlAddButton = true;
                        return this.page;
                    case"site-setup":
                        index.setValue("Site Setup");
                        index.setIcon("pi pi-fw pi-sliders-h");
                        breadcrumbModel.getElements().add(index);
                        rightPanelButton = true;
                        leftTreePanelButton = true;
                        topbarControlAddButton = true;
                        return this.page;
                }
            }
            throw new Exception();
        } catch (Exception e) {
            breadcrumbModel = new DefaultMenuModel();
            rightPanelButton = false;
            topbarControlAddButton = false;
            this.page = "home";
            return this.page;
        }
    }

    /**
     * Update dialog based on the given card
     * @param card, String Object
     */
    public void updateDialog(String card) {
        if(card != null) {
            switch(card.toLowerCase()) {
                case"empty":
                    dialogCard = "empty";
                    dialogHeader = "Empty Dialog";
                    dialogContent = "/user/card/empty.xhtml";
                    break;
                case"create-airbase":
                    dialogCard = "create-airbase";
                    dialogHeader = "Create Airbase";
                    dialogContent = "/user/card/create-airbase.xhtml";
                    if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createAirbaseBean") == null) {
                        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("createAirbaseBean", new CreateAirbaseBean());
                    }
                    ((CreateAirbaseBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createAirbaseBean")).init();
                    break;
                case"download-new-airbase":
                    dialogCard = "download-new-airbase";
                    dialogHeader = "Download Airbase";
                    dialogContent = "/user/card/download-new-airbase.xhtml";
                    break;
                case"logout":
                    dialogCard = "logout";
                    dialogHeader = "Logout";
                    dialogContent = "/user/card/logout.xhtml";
                    break;
                case "echobase-details":
                    dialogCard = "alarm-details";
                    dialogHeader = "Echobase Details";
                    dialogContent = "/user/card/echobase-details.xhtml";
                    break;
                case "device-details":
                    dialogCard = "device-details";
                    dialogHeader = "Device Details";
                    dialogContent = "/user/card/device-details.xhtml";
                    break;
                default:
                    dialogCard = "";
                    dialogHeader = "";
                    dialogContent = "/user/card/empty.xhtml";
                    break;
                    
            }
        } else {
            dialogCard = "";
            dialogHeader = "";
            dialogContent = "/user/card/empty.xhtml";
        }
        PrimeFaces.current().ajax().update("defaultDialogContainerId");
    }
    
    
    /**
     * Action to take when Dialog box closed
     */
    public void closeDialog() {
        try {
            if (dialogCard != null) {
                switch(dialogCard) {
                    case"empty":
                        break;
                    case"logout":
                        break;
                    case"create-airbase":
                        break;
                    case"download-new-airbase":
                        break;
                }
            }
        }catch (Exception e) {
            Logger.getLogger(NavigationBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }
    
    
    /**
     * Action to be taken upon the Node Expand Event
     * @param event, NodeExpandEvent Object
     */
    public void onNodeExpand(NodeExpandEvent event) {
        try {
            switch(page) {
                case"home":
                    break;
                case"maintenance":
                    break;
                case"echobases":
                    break;
                case"devices":
                    break;
                case"site-setup":
                    break;
            }
        }catch (Exception e) {
            Logger.getLogger(NavigationBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }
    
    /**
     * Action to be taken upon the Node Collapse Event
     * @param event, NodeCollapseEvent Object
     */
    public void onNodeCollapse(NodeCollapseEvent event) {
        try {
            switch(page) {
                case"home":
                    break;
                case"maintenance":
                    break;
                case"echobases":
                    break;
                case"devices":
                    break;
                case"site-setup":
                    break;
            }
        }catch (Exception e) {
            Logger.getLogger(NavigationBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    public void onNodeSelect(NodeSelectEvent event) {
        try {
            switch(page) {
                case"home":
                    break;
                case"maintenance":
                    break;
                case"echobases":
                    break;
                case"devices":
                    break;
                case"site-setup":
                    break;
            }
        }catch (Exception e) {
            Logger.getLogger(NavigationBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }
    
    public String getMenuMode() {
        return menuMode;
    }

    public MenuModel getBreadcrumbModel() {
        return breadcrumbModel;
    }

    public void setBreadcrumbModel(MenuModel breadcrumbModel) {
        this.breadcrumbModel = breadcrumbModel;
    }

    public boolean isRightPanelButton() {
        return rightPanelButton;
    }

    public boolean isLeftTreePanelButton() {
        return leftTreePanelButton;
    }

    public boolean isTopbarControlAddButton() {
        return topbarControlAddButton;
    }

    public TreeNode getTreeNodeRoot() {
        return treeNodeRoot;
    }

    public void setTreeNodeRoot(TreeNode treeNodeRoot) {
        this.treeNodeRoot = treeNodeRoot;
    }

    public TreeNode getSelectedNode() {
        return selectedNode;
    }

    public void setSelectedNode(TreeNode selectedNode) {
        this.selectedNode = selectedNode;
    }

    public String getPage() {
        return page;
    }

    public String getDialogHeader() {
        return dialogHeader;
    }

    public String getDialogContent() {
        return dialogContent;
    }

    public String getChildDialogContent() {
        return childDialogContent;
    }
}
