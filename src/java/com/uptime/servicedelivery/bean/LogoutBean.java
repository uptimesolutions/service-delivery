/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.servicedelivery.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author twilcox
 */
public class LogoutBean implements Serializable {
    public void doLogout() {
        HttpServletRequest request;
        
        try{
            request = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
            if(request.getSession(false) != null){
                request.getSession(false).invalidate();
                request.getSession(true);
                Logger.getLogger(LogoutBean.class.getName()).log(Level.INFO,"***** Invalidated Tomcat session *****");
            }
            Logger.getLogger(LogoutBean.class.getName()).log(Level.INFO,"****Redirecting to login page *****");
            /*
                This isn't complete:
                That is shown is used by worldview
                FacesContext.getCurrentInstance().getExternalContext().redirect("https://openam-dev.corp.uptime-solutions.us:8443/sso/UI/Logout");
            */
            Logger.getLogger(LogoutBean.class.getName()).log(Level.INFO,"****Redirected to login page *****");
        } catch(Exception e){
            Logger.getLogger(LogoutBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }
    
    public void keepSessionAlive(){
        HttpServletRequest request = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
        request.getSession();
        Logger.getLogger(LogoutBean.class.getName()).log(Level.INFO, "Session Extended");
    }
}
