/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.servicedelivery.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.servicedelivery.http.client.CustomerClient;
import com.uptime.servicedelivery.http.client.MonitoringClient;
import com.uptime.servicedelivery.http.client.SiteClient;
import com.uptime.servicedelivery.utils.vo.CustomersVO;
import com.uptime.servicedelivery.utils.vo.MistDiagnosticsVO;
import com.uptime.servicedelivery.utils.vo.SiteVO;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import org.primefaces.model.FilterMeta;

/**
 *
 * @author twilcox
 */
public class DevicesBean {
    private final List<CustomersVO> customerList;
    private List<SelectItem> siteList;
    private List<MistDiagnosticsVO> mistDiagnosticsList, filteredDevices;
    private List<FilterMeta> filterBy;
    private CustomersVO selectedCustomersVO;
    private SelectItem selectedSite;
    private MistDiagnosticsVO selectedMistvo;

    /**
     * Constructor
     */
    public DevicesBean() {
        customerList = new ArrayList();
        siteList = new ArrayList();
        mistDiagnosticsList = new ArrayList();
        selectedMistvo = null;
        
        try {
            customerList.addAll(CustomerClient.getInstance().getAllCustomers(ServiceRegistrarClient.getInstance().getServiceHostURL("User")));
            Logger.getLogger(DevicesBean.class.getName()).log(Level.SEVERE, "customerList.size() : {0}", customerList.size());
        } catch (Exception exp) {
            Logger.getLogger(DevicesBean.class.getName()).log(Level.SEVERE, "DevicesBean Init Exception {0}", exp.toString());
            customerList.clear();
        }
    }

    /**
     * Post Constructor
     */
    @PostConstruct
    public void init() {
        siteList.clear();
        mistDiagnosticsList.clear();
        selectedCustomersVO = new CustomersVO();
        selectedSite = null;
        selectedMistvo = null;
    }

    public void updateBean() {
        Logger.getLogger(DevicesBean.class.getName()).log(Level.INFO, "DevicesBean.updateBean() called. ***********");
    }

    public List<SelectItem> onAccountSelect() {
        List<SiteVO> list;
        
        siteList.clear();
        mistDiagnosticsList.clear();
        selectedSite = null;
        selectedMistvo = null;
        
        try {
            list = new ArrayList();
            list.addAll(SiteClient.getInstance().getSiteByCustomer(ServiceRegistrarClient.getInstance().getServiceHostURL("Config"), selectedCustomersVO.getCustomerAccount()));
            list.stream().forEachOrdered(siteVo -> siteList.add(new SelectItem(siteVo.getSiteId(), siteVo.getSiteName())));
            return siteList;
        } catch (Exception e) {
            return null;
        }
    }

    public void onSiteSelect() {
        ZonedDateTime zoneDateTime;
        MistDiagnosticsVO mist;
    
        mistDiagnosticsList.clear();
        mistDiagnosticsList.addAll(MonitoringClient.getInstance().getMistDiagnosByCustAccSite(ServiceRegistrarClient.getInstance().getServiceHostURL("Monitoring"), selectedCustomersVO.getCustomerAccount(), (UUID) selectedSite.getValue()));
        
        //set the latsSample filed for a specific zone and custmer acc name
        try {
            for (int i = 0; i < mistDiagnosticsList.size(); i++) {
                mist = mistDiagnosticsList.get(i);
            
                if (mist.getLastSample() != null) {
                    zoneDateTime = mist.getLastSample().atZone(ZoneId.of("America/New_York")).truncatedTo(ChronoUnit.SECONDS);
                    mist.setLastSampleWithSpecfZone(zoneDateTime);
                }
                mist.setCustomerAccount(selectedCustomersVO.getCustomerName());
                mist.setIndex(i);
            }
        } catch (Exception ex) {
            Logger.getLogger(DevicesBean.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    /**
     * Information of the selected MistDiagnosticsVO.
     */
    public void infoSelectedDevice() {
        String tmp;
        int index;
        
        try {
            if ((tmp = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("deviceResultFormId:indexId")) != null) {
                index = Integer.parseInt(tmp);
                selectedMistvo = new MistDiagnosticsVO(mistDiagnosticsList.stream().filter(mist -> mist.getIndex() == index).findFirst().get());
            } else {
                selectedMistvo = new MistDiagnosticsVO();
            }
        } catch (Exception ex) {
            Logger.getLogger(DevicesBean.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            selectedMistvo = new MistDiagnosticsVO();
        }
        ((NavigationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("navigationBean")).updateDialog("device-details");
    }

    /**
     * Query the customersVO list for all items that contain the given value
     *
     * @param query, String Object
     * @return List Object of CustomersVO Objects
     */
    public List<CustomersVO> queryCustomerList(String query) {
        try {
            if (query == null || query.isEmpty()) {
                return customerList;
            } else {
                return customerList.stream().filter(item -> item.getCustomerName().toLowerCase().contains(query.toLowerCase())).sorted().collect(Collectors.toList());
            }
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Query the SiteVO list for all items that contain the given value
     *
     * @param query, String Object
     * @return List Object of SiteVO Objects
     */
    public List<SelectItem> querySiteList(String query) {
        try {
            if (query == null || query.isEmpty()) {
                return siteList;
            } else {
                return siteList.stream().filter(item -> item.getLabel().toLowerCase().contains(query.toLowerCase())).sorted(Comparator.comparing(SelectItem::getLabel)).collect(Collectors.toList());
            }
        } catch (Exception e) {
            return null;
        }
    }
    
    /**
     * List of MistDiagnosticsVO based on BaseStationHostName query.
     *
     * @param query
     * @return List of String
     */
    public List<String> queryBaseStationHostnameList(String query) {
        try {
            return mistDiagnosticsList.stream().filter(mist -> mist.getBaseStationHostName().toLowerCase().contains(query.toLowerCase())).map(MistDiagnosticsVO::getBaseStationHostName).distinct().sorted().collect(Collectors.toList());
        } catch (Exception e) {
            return null;
        }
    }
    
    /**
     * List of MistDiagnosticsVO based on EchoDeviceId query.
     *
     * @param query
     * @return List of String
     */
    public List<String> queryEchoDeviceIdList(String query) {
        try {
            return mistDiagnosticsList.stream().filter(mist -> mist.getEchoDeviceId().toLowerCase().contains(query.toLowerCase())).map(MistDiagnosticsVO::getEchoDeviceId).distinct().sorted().collect(Collectors.toList());
        } catch (Exception e) {
            return null;
        }
    }
    
    /**
     * List of MistDiagnosticsVO based on DeviceId query.
     *
     * @param query
     * @return List of String
     */
    public List<String> queryDeviceIdList(String query) {
        try {
            return mistDiagnosticsList.stream().filter(mist -> mist.getDeviceId().toLowerCase().contains(query.toLowerCase())).map(MistDiagnosticsVO::getDeviceId).distinct().sorted().collect(Collectors.toList());
        } catch (Exception e) {
            return null;
        }
    }
    
    /**
     * List of MistDiagnosticsVO based on AreaName query.
     *
     * @param query
     * @return List of String
     */
    public List<String> queryAreaList(String query) {
        try {
            return mistDiagnosticsList.stream().filter(mist -> mist.getAreaName().toLowerCase().contains(query.toLowerCase())).map(MistDiagnosticsVO::getAreaName).distinct().sorted().collect(Collectors.toList());
        } catch (Exception e) {
            return null;
        }
    }
    
    /**
     * List of MistDiagnosticsVO based on AssetName query.
     *
     * @param query
     * @return List of String
     */
    public List<String> queryAssetList(String query) {
        try {
            return mistDiagnosticsList.stream().filter(mist -> mist.getAssetName().toLowerCase().contains(query.toLowerCase())).map(MistDiagnosticsVO::getAssetName).distinct().sorted().collect(Collectors.toList());
        } catch (Exception e) {
            return null;
        }
    }
    
    /**
     * List of MistDiagnosticsVO based on PointLocationName query.
     *
     * @param query
     * @return List of String
     */
    public List<String> queryPointLocationList(String query) {
        try {
            return mistDiagnosticsList.stream().filter(mist -> mist.getPointLocationName().toLowerCase().contains(query.toLowerCase())).map(MistDiagnosticsVO::getPointLocationName).distinct().sorted().collect(Collectors.toList());
        } catch (Exception e) {
            return null;
        }
    }
    
    /**
     * List of MistDiagnosticsVO based on PointName query.
     *
     * @param query
     * @return List of String
     */
    public List<String> queryPointList(String query) {
        try {
            return mistDiagnosticsList.stream().filter(mist -> mist.getPointName().toLowerCase().contains(query.toLowerCase())).map(MistDiagnosticsVO::getPointName).distinct().sorted().collect(Collectors.toList());
        } catch (Exception e) {
            return null;
        }
    }
    
    /**
     * List of MistDiagnosticsVO based on FormatedLastSample query.
     *
     * @param query
     * @return List of String
     */
    public List<String> queryFormatedLastSampleList(String query) {
        try {
            return mistDiagnosticsList.stream().filter(mist -> mist.getFormatedLastSample().toLowerCase().contains(query.toLowerCase())).map(MistDiagnosticsVO::getFormatedLastSample).distinct().sorted().collect(Collectors.toList());
        } catch (Exception e) {
            return null;
        }
    }

    public CustomersVO getSelectedCustomersVO() {
        return selectedCustomersVO;
    }

    public void setSelectedCustomersVO(CustomersVO selectedCustomersVO) {
        this.selectedCustomersVO = selectedCustomersVO;
    }

    public SelectItem getSelectedSite() {
        return selectedSite;
    }

    public void setSelectedSite(SelectItem selectedSite) {
        this.selectedSite = selectedSite;
    }

    public List<SelectItem> getSiteList() {
        return siteList;
    }

    public List<MistDiagnosticsVO> getMistDiagnosticsList() {
        return mistDiagnosticsList;
    }

    public MistDiagnosticsVO getSelectedMistvo() {
        return selectedMistvo;
    }

    public List<MistDiagnosticsVO> getFilteredDevices() {
        return filteredDevices;
    }

    public void setFilteredDevices(List<MistDiagnosticsVO> filteredDevices) {
        this.filteredDevices = filteredDevices;
    }

    public List<FilterMeta> getFilterBy() {
        return filterBy;
    }

    public void setFilterBy(List<FilterMeta> filterBy) {
        this.filterBy = filterBy;
    }

}
