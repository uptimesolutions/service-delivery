/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.servicedelivery.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.servicedelivery.http.client.CustomerClient;
import com.uptime.servicedelivery.http.client.MonitoringClient;
import com.uptime.servicedelivery.http.client.SiteClient;
import com.uptime.servicedelivery.utils.vo.CustomersVO;
import com.uptime.servicedelivery.utils.vo.EchoDiagnosticsVO;
import com.uptime.servicedelivery.utils.vo.SiteVO;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

/**
 *
 * @author twilcox
 */
public class EchobasesBean {
    private final List<CustomersVO> customerList;
    private List<SelectItem> siteList;
    private List<EchoDiagnosticsVO> echoDiagnosticsList, filteredEchobases;
    private CustomersVO selectedCustomersVO;
    private SelectItem selectedSite;
    private EchoDiagnosticsVO selectedEchoDiagnosticsVO;

    /**
     * Constructor
     */
    public EchobasesBean() {
        customerList = new ArrayList();
        siteList = new ArrayList();
        echoDiagnosticsList = new ArrayList();
        
        try {
            customerList.addAll(CustomerClient.getInstance().getAllCustomers(ServiceRegistrarClient.getInstance().getServiceHostURL("User")));
            Logger.getLogger(EchobasesBean.class.getName()).log(Level.SEVERE, "customerList.size() : {0}", customerList.size());
        } catch (Exception exp) {
            Logger.getLogger(EchobasesBean.class.getName()).log(Level.SEVERE, "EchobasesBean Init Exception {0}", exp.toString());
            customerList.clear();
        }
    }

    /**
     * Post Constructor
     */
    @PostConstruct
    public void init() {
        siteList.clear();
        echoDiagnosticsList.clear();
        selectedCustomersVO = new CustomersVO();
        selectedSite = null;
        selectedEchoDiagnosticsVO= null;
    }

    public void updateBean() {
        Logger.getLogger(EchobasesBean.class.getName()).log(Level.INFO, "EchobasesBean.updateBean() called. ***********");
    }

    public List<SelectItem> onAccountSelect() {
        List<SiteVO> list;
        
        siteList.clear();
        echoDiagnosticsList.clear();
        selectedSite = null;
        
        try {
            list = new ArrayList();
            list.addAll(SiteClient.getInstance().getSiteByCustomer(ServiceRegistrarClient.getInstance().getServiceHostURL("Config"), selectedCustomersVO.getCustomerAccount()));
            list.stream().forEachOrdered(siteVo -> siteList.add(new SelectItem(siteVo.getSiteId(), siteVo.getSiteName())));
            return siteList;
        } catch (Exception e) {
            return null;
        }
    }

    public void onSiteSelect() {
        ZonedDateTime zoneDateTime;
        EchoDiagnosticsVO echo;
        
        echoDiagnosticsList.clear();
        echoDiagnosticsList.addAll(MonitoringClient.getInstance().getEchoDiagnosByCustAccSite(ServiceRegistrarClient.getInstance().getServiceHostURL("Monitoring"), selectedCustomersVO.getCustomerAccount(), (UUID) selectedSite.getValue()));
        
        //set the latsSample filed for a specific zone and custmer acc name.
        try {
            for (int i = 0; i < echoDiagnosticsList.size(); i++) {
                echo = echoDiagnosticsList.get(i);

                if (echo.getLastUpdate() != null) {
                    zoneDateTime = echo.getLastUpdate().atZone(ZoneId.of("America/New_York")).truncatedTo(ChronoUnit.SECONDS);
                    echo.setLastUpdateWithSpecfZone(zoneDateTime);
                }
                echo.setCustomerAccount(selectedCustomersVO.getCustomerName());
                echo.setIndex(i);
            }
        } catch (Exception ex) {
            Logger.getLogger(EchobasesBean.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    /**
     * Information of the selected MistDiagnosticsVO.
     */
    public void infoOfSelectedEchoDiag() {
        String tmp;
        int index;
        
        try {
            if ((tmp = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("echobaseResultFormId:indexId")) != null) {
                index = Integer.parseInt(tmp);
                selectedEchoDiagnosticsVO = new EchoDiagnosticsVO(echoDiagnosticsList.stream().filter(echo -> echo.getIndex() == index).findFirst().get());
            } else {
                selectedEchoDiagnosticsVO = new EchoDiagnosticsVO();
            }
        } catch (Exception ex) {
            Logger.getLogger(EchobasesBean.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            selectedEchoDiagnosticsVO = new EchoDiagnosticsVO();
        }
        ((NavigationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("navigationBean")).updateDialog("echobase-details");
    }

    /**
     * Query the customersVO list for all items that contain the given value
     * @param query, String Object
     * @return List Object of CustomersVO Objects
     */
    public List<CustomersVO> queryCustomerList(String query) {
        try {
            if (query == null || query.isEmpty()) {
                return customerList;
            } else {
                return customerList.stream().filter(item -> item.getCustomerName().toLowerCase().contains(query.toLowerCase())).sorted().collect(Collectors.toList());
            }
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Query the SiteVO list for all items that contain the given value
     *
     * @param query, String Object
     * @return List Object of SiteVO Objects
     */
    public List<SelectItem> querySiteList(String query) {
        try {
            if (query == null || query.isEmpty()) {
                return siteList;
            } else {
                return siteList.stream().filter(item -> item.getLabel().toLowerCase().contains(query.toLowerCase())).sorted(Comparator.comparing(SelectItem::getLabel)).collect(Collectors.toList());
            }
        } catch (Exception e) {
            return null;
        }
    }
    
    /**
     * List of EchoDiagnosticsVO based on BaseStationHostName query.
     *
     * @param query
     * @return List of String
     */
    public List<String> queryBaseStationHostnameList(String query) {
        try {
            return echoDiagnosticsList.stream().filter(echo -> echo.getBaseStationHostName().toLowerCase().contains(query.toLowerCase())).map(EchoDiagnosticsVO::getBaseStationHostName).distinct().sorted().collect(Collectors.toList());
        } catch (Exception e) {
            return null;
        }
    }
    
    /**
     * List of EchoDiagnosticsVO based on DeviceId query.
     *
     * @param query
     * @return List of String
     */
    public List<String> queryDeviceIdList(String query) {
        try {
            return echoDiagnosticsList.stream().filter(echo -> echo.getDeviceId().toLowerCase().contains(query.toLowerCase())).map(EchoDiagnosticsVO::getDeviceId).distinct().sorted().collect(Collectors.toList());
        } catch (Exception e) {
            return null;
        }
    }
    
    /**
     * List of EchoDiagnosticsVO based on EchoFw query.
     *
     * @param query
     * @return List of String
     */
    public List<String> queryEchoFwList(String query) {
        try {
            return echoDiagnosticsList.stream().filter(echo -> echo.getEchoFw().toLowerCase().contains(query.toLowerCase())).map(EchoDiagnosticsVO::getEchoFw).distinct().sorted().collect(Collectors.toList());
        } catch (Exception e) {
            return null;
        }
    }
    
    /**
     * List of EchoDiagnosticsVO based on FormatedLastUpdate query.
     *
     * @param query
     * @return List of String
     */
    public List<String> queryFormatedLastUpdateList(String query) {
        try {
            return echoDiagnosticsList.stream().filter(echo -> echo.getFormatedLastUpdate().toLowerCase().contains(query.toLowerCase())).map(EchoDiagnosticsVO::getFormatedLastUpdate).distinct().sorted().collect(Collectors.toList());
        } catch (Exception e) {
            return null;
        }
    }

    public CustomersVO getSelectedCustomersVO() {
        return selectedCustomersVO;
    }

    public void setSelectedCustomersVO(CustomersVO selectedCustomersVO) {
        this.selectedCustomersVO = selectedCustomersVO;
    }

    public SelectItem getSelectedSite() {
        return selectedSite;
    }

    public void setSelectedSite(SelectItem selectedSite) {
        this.selectedSite = selectedSite;
    }

    public List<SelectItem> getSiteList() {
        return siteList;
    }

    public List<EchoDiagnosticsVO> getEchoDiagnosticsList() {
        return echoDiagnosticsList;
    }

    public EchoDiagnosticsVO getSelectedEchoDiagnosticsVO() {
        return selectedEchoDiagnosticsVO;
    }

    public List<EchoDiagnosticsVO> getFilteredEchobases() {
        return filteredEchobases;
    }

    public void setFilteredEchobases(List<EchoDiagnosticsVO> filteredEchobases) {
        this.filteredEchobases = filteredEchobases;
    }

}
