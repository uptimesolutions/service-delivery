/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.servicedelivery.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.servicedelivery.http.client.ConfigClient;
import com.uptime.servicedelivery.http.client.MonitoringClient;
import com.uptime.servicedelivery.utils.vo.ApprovedBaseStationsVO;
import com.uptime.servicedelivery.utils.vo.SiteCustomerVO;
import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author gsingh
 */
public class CreateAirbaseBean implements Serializable {
    private ApprovedBaseStationsVO approvedBaseStationsVO;
    private final List<String> customerList;
    private final List<SiteCustomerVO> siteList;

    /**
     * Constructor
     */
    public CreateAirbaseBean() {
        customerList = new ArrayList();
        siteList = new ArrayList();
        
        try {
            customerList.addAll(((AccountManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("accountManageBean")).getCustomerAccounts());
        } catch (Exception e) {
            Logger.getLogger(CreateAirbaseBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            customerList.clear();
        }
    }

    /**
     * Post Constructor
     */
    @PostConstruct
    public void init() {
        resetPage();
    }

    /**
     * Resets the page
     */
    public void resetPage() {
        approvedBaseStationsVO = new ApprovedBaseStationsVO();
        siteList.clear();
    }

    /**
     * Called when the Customer Account is changed
     */
    public void onCustomerChange() {
        String customerAccount;
        
        customerAccount = approvedBaseStationsVO.getCustomerAccount();
        approvedBaseStationsVO = new ApprovedBaseStationsVO();
        siteList.clear();
        if (customerAccount != null && !customerAccount.isEmpty()) {
            approvedBaseStationsVO.setCustomerAccount(customerAccount);
            siteList.addAll(ConfigClient.getInstance().getSiteCustomerByCustomer(ServiceRegistrarClient.getInstance().getServiceHostURL("Config"), customerAccount).stream().filter(vo -> vo.getSiteName() != null && !vo.getSiteName().isEmpty()).sorted(Comparator.comparing(SiteCustomerVO::getSiteName)).collect(Collectors.toList()));
        }
    }

    /**
     * Adds a new ApprovedBaseStations Object to Cassandra, if it doesn't already exist
     */
    public void createAirbase() {
        if (!MonitoringClient.getInstance().getApprovedBaseStationsByPK(ServiceRegistrarClient.getInstance().getServiceHostURL("Monitoring"), approvedBaseStationsVO.getCustomerAccount(), approvedBaseStationsVO.getSiteId(), approvedBaseStationsVO.getMacAddress()).isEmpty()) {
            FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage("Provided details already exists. Try another?"));
            return;
        }
        
        approvedBaseStationsVO.setPasscode(UUID.randomUUID());
        if (MonitoringClient.getInstance().createApprovedBaseStations(ServiceRegistrarClient.getInstance().getServiceHostURL("Monitoring"), approvedBaseStationsVO)) {
            ((NavigationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("navigationBean")).updateDialog("download-new-airbase");
            PrimeFaces.current().executeScript("PF('defaultDlg').show()");
        } else {
            FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage("Something went wrong. Contact IT Team."));
        }
    }

    /**
     * Download the current info of the recently created ApprovedBaseStations Object
     * @param approvedBaseStationsVO, ApprovedBaseStationsVO Object
     * @return StreamedContent Object
     */
    public StreamedContent download(ApprovedBaseStationsVO approvedBaseStationsVO) {
        StringBuilder content;
        
        try {
            content = new StringBuilder();
            content
                    .append("POLLING_FILES_DIRECTORY_PATH=\n")
                    .append("MAXIMUM_NUMBER_OF_WORKER_THREADS=\n")
                    .append("BASE_STATION_WEB_SERVICE_URI=\n")
                    .append("POLLING_INTERVAL=5\n")
                    .append("CUSTOMER_ACCOUNT=").append(approvedBaseStationsVO.getCustomerAccount()).append("\n")
                    .append("SITE_ID=").append(approvedBaseStationsVO.getSiteId()).append("\n")
                    .append("SITE_NAME=").append(siteList.stream().filter(vo -> vo.getSiteId().equals(approvedBaseStationsVO.getSiteId())).map(SiteCustomerVO::getSiteName).findFirst().get()).append("\n")
                    .append("HARDWARE_TOKEN=\n")
                    .append("PASSCODE=").append(approvedBaseStationsVO.getPasscode());
            
            return DefaultStreamedContent.builder()
                    .name("airbaseDetails_" + approvedBaseStationsVO.getMacAddress().replaceAll(":", "_") + ".properties")
                    .contentType("application/text")
                    .stream(() -> new ByteArrayInputStream(content.toString().getBytes()))
                    .build();
            
        } catch (Exception e) {
            Logger.getLogger(CreateAirbaseBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        
        return null;
    }
    

    public ApprovedBaseStationsVO getApprovedBaseStationsVO() {
        return approvedBaseStationsVO;
    }

    public List<String> getCustomerList() {
        return customerList;
    }

    public List<SiteCustomerVO> getSiteList() {
        return siteList;
    }
}
