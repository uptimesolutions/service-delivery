/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.servicedelivery.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author twilcox
 */
public class AccountManageBean implements Serializable {
    private String userName, role;
    private final List<String> customerAccounts;

    /**
     * Constructor
     */
    public AccountManageBean() {
        HttpServletRequest request;
        
        try {
            request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
            
            userName = request.getHeader("displayName");
            userName = userName == null ? "Sam Spade" : userName;
            
            role = request.getHeader("role");
            role = role == null ? "Admin" : role;
        } catch(Exception e) {
            Logger.getLogger(AccountManageBean.class.getName()).log(Level.SEVERE, "AccountManageBean Constructor Exception {0}", e.toString());
            userName = "Sam Spade";
            role = "Admin";
        }
        
        //*************************************
        //      Place Holder
            customerAccounts = new ArrayList();
            customerAccounts.add("77777");
            customerAccounts.add("FedEx");
            customerAccounts.add("Fedex Express");
            customerAccounts.add("FEDEX EXPRESS");
        //*************************************
    }

    public String getUserName() {
        return userName;
    }

    public String getRole() {
        return role;
    }

    public List<String> getCustomerAccounts() {
        return customerAccounts;
    }
}
