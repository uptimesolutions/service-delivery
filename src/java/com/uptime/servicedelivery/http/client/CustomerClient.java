/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.servicedelivery.http.client;

import com.uptime.client.http.client.Client;
import com.uptime.client.utils.vo.HttpResponseVO;
import com.uptime.servicedelivery.http.utils.json.dom.CustomerDomJsonParser;
import com.uptime.servicedelivery.http.utils.json.sax.CustomerSaxJsonParser;
import com.uptime.servicedelivery.utils.vo.CustomersVO;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author twilcox
 */
public class CustomerClient extends Client {
    private static CustomerClient instance = null;

    /**
     * Private Singleton class
     */
    private CustomerClient() {
    }

    /**
     * Returns an instance of the class
     *
     * @return CustomerClient object
     */
    public static CustomerClient getInstance() {
        if (instance == null) {
            instance = new CustomerClient();
        }
        return instance;
    }
    
    /**
     * Get all rows from the customers table
     * @param host, String Object
     * @return List Object of CustomersVO Objects
     */
    public List<CustomersVO> getAllCustomers(String host) {
        List<CustomersVO> list = new ArrayList();
        HttpResponseVO responseVO;
        
        try {
            if ((responseVO = httpGet("User", host, "/user/customers?customerAccount=ALL")) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        list = CustomerSaxJsonParser.getInstance().populateCustomersVOFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        Logger.getLogger(CustomerClient.class.getName()).log(Level.WARNING, "Message from User Preference Service: {0}", CustomerDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(CustomerClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(CustomerClient.class.getName()).log(Level.WARNING, "Error Message from User Preference Service: {0}", CustomerDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
        } catch (Exception e) {
            Logger.getLogger(CustomerClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }
    
    /**
     * Get a row from the customers table based on the PK
     * @param host, String Object
     * @param customerAccount, String Object
     * @return List Object of CustomersVO Objects
     */
    public CustomersVO getCustomersByPK(String host, String customerAccount) {
        HttpResponseVO responseVO;
        
        try {
            if ((responseVO = httpGet("User", host, "/user/customers?customerAccount=" + URLEncoder.encode(customerAccount, "UTF-8"))) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        return CustomerSaxJsonParser.getInstance().populateCustomersVOFromJson(responseVO.getEntity()).get(0);
                    case 404:
                        Logger.getLogger(CustomerClient.class.getName()).log(Level.WARNING, "Message from User Preference Service: {0}", CustomerDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(CustomerClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(CustomerClient.class.getName()).log(Level.WARNING, "Error Message from User Preference Service: {0}", CustomerDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
        } catch (Exception e) {
            Logger.getLogger(CustomerClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        return null;
    }
}
