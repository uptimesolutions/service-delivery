/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.servicedelivery.http.client;

import com.uptime.client.http.client.Client;
import com.uptime.client.utils.vo.HttpResponseVO;
import com.uptime.servicedelivery.http.utils.json.dom.SiteDomJsonParser;
import com.uptime.servicedelivery.http.utils.json.sax.SiteSaxJsonParser;
import static com.uptime.servicedelivery.utils.helperclass.GenericMessage.showErrorMsg;
import com.uptime.servicedelivery.utils.vo.SiteContactVO;
import com.uptime.servicedelivery.utils.vo.SiteVO;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kpati
 */
public class SiteClient extends Client {

    private static SiteClient instance = null;
    
    /**
     * Private Singleton class
     */
    private SiteClient() {
    }

    /**
     * Returns an instance of the class
     *
     * @return SiteClient object
     */
    public static SiteClient getInstance() {
        if (instance == null) {
            instance = new SiteClient();
        }
        return instance;
    }

    /**
     * Get Site Info based on the given info
     *
     * @param host String Object
     * @param customer, String Object
     * @return List Object of SiteVO Objects
     */
    public List<SiteVO> getSiteByCustomer(String host, String customer) {
        List<SiteVO> list = null;
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;
        
        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/config/site")
                    .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&SiteCustomer=True");
            if ((responseVO = httpGet("Config", host, endPoint.toString())) != null && responseVO.isHostFound()) {

                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = SiteSaxJsonParser.getInstance().populateSiteFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(SiteClient.class.getName()).log(Level.WARNING, "Message from Config Service: {0}", SiteDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(SiteClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(SiteClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", SiteDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(SiteClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            return list;
        }
        return list;
    }
    
    /**
     * Get Site Info based on the given info
     *
     * @param host String Object
     * @param customer, String Object
     * @param siteId
     * @return List Object of SiteVO Objects
     */
    public List<SiteContactVO> getSiteContactsByCustomerSite(String host, String customer, UUID siteId) {
        List<SiteContactVO> list = null;
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;
        
        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/config/site")
                    .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId)
                    .append("&SiteContacts=True");
            if ((responseVO = httpGet("Config", host, endPoint.toString())) != null && responseVO.isHostFound()) {

                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = SiteSaxJsonParser.getInstance().populateSiteContactsFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(SiteClient.class.getName()).log(Level.WARNING, "Message from Config Service: {0}", SiteDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(SiteClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(SiteClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", SiteDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(SiteClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            return list;
        }
        return list;
    }


    /**
     * Get Site Info based on the given info
     *
     * @param host String Object
     * @param customer, String Object
     * @return List Object of SiteVO Objects
     */
    public List<SiteVO> getSiteRegionCountryByCustomer(String host, String customer) {
        List<SiteVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;
        
        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/config/site")
                    .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&SiteRegionCountry=True");
            if ((responseVO = httpGet("Config", host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = SiteSaxJsonParser.getInstance().populateSiteFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(SiteClient.class.getName()).log(Level.WARNING, "Message from Config Service: {0}", SiteDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(SiteClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(SiteClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", SiteDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(SiteClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            return list;
        }
        return list;
    }

    /**
     * Get Site Info based on the given info
     *
     * @param host String Object
     * @param customer, String Object
     * @param region, String Object
     * @param country, String Object
     * @return List Object of SiteVO Objects
     */
    public List<SiteVO> getSiteRegionCountryByCustomerRegionCountry(String host, String customer, String region, String country) {
        List<SiteVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;
        
        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/config/site")
                    .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&region=").append(URLEncoder.encode(region, "UTF-8"))
                    .append("&country=").append(URLEncoder.encode(country, "UTF-8"))
                    .append("&SiteRegionCountry=True");
            if ((responseVO = httpGet("Config", host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = SiteSaxJsonParser.getInstance().populateSiteFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(SiteClient.class.getName()).log(Level.WARNING, "Message from Config Service: {0}", SiteDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(SiteClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(SiteClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", SiteDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(SiteClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            return list;
        }
        return list;
    }

    /**
     * Create the given Site
     *
     * @param host, String Object
     * @param siteVO, SiteVO Object
     * @return boolean, true if success, else false
     */
    public boolean createSite(String host, SiteVO siteVO) {
        HttpResponseVO responseVO;
        boolean serviceIssue = true;
        
        Logger.getLogger(SiteClient.class.getName()).log(Level.INFO, "SiteDomJsonParser JSON**************: {0}", SiteDomJsonParser.getInstance().getJsonFromSite(siteVO));
        if ((responseVO = httpPost("Config", host, "/config/site", SiteDomJsonParser.getInstance().getJsonFromSite(siteVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
//                UserManageBean userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
//                FacesContext.getCurrentInstance().addMessage("nonAutoGrowlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_create_site_success_message")));
                Logger.getLogger(SiteClient.class.getName()).log(Level.INFO, "Site successfully created.");
                return true;
            } else {
                Logger.getLogger(SiteClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(SiteClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", SiteDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        if (serviceIssue) {
            showErrorMsg();
        }
        return false;
    }

    
}
