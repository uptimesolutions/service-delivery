/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.servicedelivery.http.client;

import com.uptime.client.http.client.Client;
import com.uptime.client.utils.vo.HttpResponseVO;
import com.uptime.servicedelivery.http.utils.json.dom.ConfigDomJsonParser;
import com.uptime.servicedelivery.http.utils.json.sax.ConfigSaxJsonParser;
import com.uptime.servicedelivery.utils.vo.SiteCustomerVO;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author twilcox
 */
public class ConfigClient extends Client {
    private static ConfigClient instance = null;

    /**
     * Private Singleton class
     */
    private ConfigClient() {
    }

    /**
     * Returns an instance of the class
     *
     * @return ConfigClient object
     */
    public static ConfigClient getInstance() {
        if (instance == null) {
            instance = new ConfigClient();
        }
        return instance;
    }

    /**
     * Get SiteCustomer Info based on the given info
     *
     * @param host String Object
     * @param customer, String Object
     * @param siteId, UUID Object
     * @return List Object of SiteCustomerVO Objects
     */
    public List<SiteCustomerVO> getSiteCustomerByPK(String host, String customer, UUID siteId) {
        List<SiteCustomerVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        String errMsg;
        
        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/config/site")
                    .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId)
                    .append("&SiteCustomer=True");
            if ((responseVO = httpGet("Config", host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        list = ConfigSaxJsonParser.getInstance().populateSiteCustomerFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        Logger.getLogger(ConfigClient.class.getName()).log(Level.WARNING, "Message from Config Service: {0}", ConfigDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(ConfigClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(ConfigClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", (errMsg = ConfigDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity())));
                        FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage("Error Message from Config Service: " + errMsg));
                }
            }
        } catch (Exception e) {
            Logger.getLogger(ConfigClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }

    /**
     * Get SiteCustomer Info based on the given info
     *
     * @param host String Object
     * @param customer, String Object
     * @return List Object of SiteCustomerVO Objects
     */
    public List<SiteCustomerVO> getSiteCustomerByCustomer(String host, String customer) {
        List<SiteCustomerVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        String errMsg;
        
        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/config/site")
                    .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&SiteCustomer=True");
            if ((responseVO = httpGet("Config", host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        list = ConfigSaxJsonParser.getInstance().populateSiteCustomerFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        Logger.getLogger(ConfigClient.class.getName()).log(Level.WARNING, "Message from Config Service: {0}", ConfigDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(ConfigClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(ConfigClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", (errMsg = ConfigDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity())));
                        FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage("Error Message from Config Service: " + errMsg));
                }
            }
        } catch (Exception e) {
            Logger.getLogger(ConfigClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }
}
