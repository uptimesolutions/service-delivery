/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.servicedelivery.http.client;

import com.uptime.client.http.client.Client;
import com.uptime.client.utils.vo.HttpResponseVO;
import com.uptime.servicedelivery.http.utils.json.dom.MonitoringDomJsonParser;
import com.uptime.servicedelivery.http.utils.json.sax.MonitoringSaxJsonParser;
import com.uptime.servicedelivery.utils.vo.ApprovedBaseStationsVO;
import com.uptime.servicedelivery.utils.vo.EchoDiagnosticsVO;
import com.uptime.servicedelivery.utils.vo.MistDiagnosticsVO;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author gsingh
 */
public class MonitoringClient extends Client {
    private static MonitoringClient instance = null;

    /**
     * Private Singleton class
     */
    private MonitoringClient() {
    }

    /**
     * Returns an instance of the class
     *
     * @return MonitoringClient object
     */
    public static MonitoringClient getInstance() {
        if (instance == null) {
            instance = new MonitoringClient();
        }
        return instance;
    }
    
        /**
     * Get Airbase Info based on the given info
     *
     * @param host String Object
     * @param customer, String Object
     * @param siteId, UUID Object
     * @return List Object of EchoDiagnosticsVO Objects
     */
    public List<EchoDiagnosticsVO> getEchoDiagnosByCustAccSite(String host, String customer, UUID siteId) {
        List<EchoDiagnosticsVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        String errMsg;
        
        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/device-telemetry")
                    .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId)
                    .append("&echoDiagnostics=").append("True");
            if ((responseVO = httpGet("Monitoring", host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        list = MonitoringSaxJsonParser.getInstance().populateEchoDiagnosFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        Logger.getLogger(MonitoringClient.class.getName()).log(Level.WARNING, "Message from Monitoring Service: {0}", MonitoringDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(MonitoringClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(MonitoringClient.class.getName()).log(Level.WARNING, "Error Message from Monitoring Service: {0}", (errMsg = MonitoringDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity())));
                        FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage("Error Message from Monitoring Service: " + errMsg));
                }
            }
        } catch (Exception e) {
            Logger.getLogger(MonitoringClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }
    
        /**
     * Get Airbase Info based on the given info
     *
     * @param host String Object
     * @param customer, String Object
     * @param siteId, UUID Object
     * @return List Object of MistDiagnosticsVO Objects
     */
    public List<MistDiagnosticsVO> getMistDiagnosByCustAccSite(String host, String customer, UUID siteId) {
        List<MistDiagnosticsVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        String errMsg;
        
        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/device-telemetry")
                    .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId)
                    .append("&mistDiagnostics=").append("True");
            if ((responseVO = httpGet("Monitoring", host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        list = MonitoringSaxJsonParser.getInstance().populateMistDiagnosFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        Logger.getLogger(MonitoringClient.class.getName()).log(Level.WARNING, "Message from Monitoring Service: {0}", MonitoringDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(MonitoringClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(MonitoringClient.class.getName()).log(Level.WARNING, "Error Message from Monitoring Service: {0}", (errMsg = MonitoringDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity())));
                        FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage("Error Message from Monitoring Service: " + errMsg));
                }
            }
        } catch (UnsupportedEncodingException e) {
            Logger.getLogger(MonitoringClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }

    
    /**
     * Get Airbase Info based on the given info
     *
     * @param host String Object
     * @param customer, String Object
     * @param siteId, UUID Object
     * @param macAddress, String Object
     * @return List Object of ApprovedBaseStationsVO Objects
     */
    public List<ApprovedBaseStationsVO> getApprovedBaseStationsByPK(String host, String customer, UUID siteId, String macAddress) {
        List<ApprovedBaseStationsVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        String errMsg;
        
        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/monitor")
                    .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId)
                    .append("&mac=").append(URLEncoder.encode(macAddress, "UTF-8"));
            if ((responseVO = httpGet("Monitoring", host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        list = MonitoringSaxJsonParser.getInstance().populateApprovedBaseStationsFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        Logger.getLogger(MonitoringClient.class.getName()).log(Level.WARNING, "Message from Monitoring Service: {0}", MonitoringDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(MonitoringClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(MonitoringClient.class.getName()).log(Level.WARNING, "Error Message from Monitoring Service: {0}", (errMsg = MonitoringDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity())));
                        FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage("Error Message from Monitoring Service: " + errMsg));
                }
            }
        } catch (Exception e) {
            Logger.getLogger(MonitoringClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }

    /**
     * Get Airbase Info based on the given info
     *
     * @param host String Object
     * @param customer, String Object
     * @param siteId, UUID Object
     * @return List Object of ApprovedBaseStationsVO Objects
     */
    public List<ApprovedBaseStationsVO> getApprovedBaseStationsByCustomerSite(String host, String customer, UUID siteId) {
        List<ApprovedBaseStationsVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        String errMsg;
        
        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/monitor")
                    .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId);
            if ((responseVO = httpGet("Monitoring", host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        list = MonitoringSaxJsonParser.getInstance().populateApprovedBaseStationsFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        Logger.getLogger(MonitoringClient.class.getName()).log(Level.WARNING, "Message from Monitoring Service: {0}", MonitoringDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(MonitoringClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(MonitoringClient.class.getName()).log(Level.WARNING, "Error Message from Monitoring Service: {0}", (errMsg = MonitoringDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity())));
                        FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage("Error Message from Monitoring Service: " + errMsg));
                }
            }
        } catch (Exception e) {
            Logger.getLogger(MonitoringClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }
    
    /**
     * @param host String Object
     * @param airbaseVO, ApprovedBaseStationsVO Object
     * @return 
     */
    public boolean createApprovedBaseStations(String host, ApprovedBaseStationsVO airbaseVO) {
        HttpResponseVO responseVO;
        String errMsg;

        if ((responseVO = httpPost("Monitoring", host, "/monitor", MonitoringDomJsonParser.getInstance().getJsonFromMonitoring(airbaseVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage("Approved Base Station created successfully."));
                Logger.getLogger(MonitoringClient.class.getName()).log(Level.INFO, "Approved Base Station created successfully.");
                return true;
            } else {
                Logger.getLogger(MonitoringClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(MonitoringClient.class.getName()).log(Level.WARNING, "Error Message from Monitoring Service: {0}", (errMsg = MonitoringDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity())));
                FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage("Error Message from Monitoring Service: " + errMsg));
            }
        }
        return false;
    }
}
