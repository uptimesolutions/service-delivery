/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.servicedelivery.http.utils.json.sax;

import com.uptime.client.utils.SaxJsonParser;
import com.uptime.servicedelivery.utils.vo.CustomersVO;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;

/**
 *
 * @author twilcox
 */
public class CustomerSaxJsonParser extends SaxJsonParser {
    private static CustomerSaxJsonParser instance = null;
    
    /**
     * Private Singleton class
     */
    private CustomerSaxJsonParser() {
    }

    /**
     * Returns an instance of the class
     * @return CustomerSaxJsonParser object
     */
    public static CustomerSaxJsonParser getInstance() {
        if (instance == null) {
            instance = new CustomerSaxJsonParser();
        }
        return instance; 
    }

    /**
     * Converts a json to a List Object of ApAlSetVO Objects
     * @param content, String Object, json 
     * @return List Object of ApAlSetVO Objects
     */
    public List<CustomersVO> populateCustomersVOFromJson(String content) {
        List<CustomersVO> list = new ArrayList();
        String keyName;
        CustomersVO customersVO;
        boolean inArray;
         
        try {
            parser = Json.createParser(new StringReader(content));
            keyName = null;
            customersVO = null;
            inArray = false;
            
            while (parser.hasNext()) {
                switch((event = parser.next())) {
                    case START_ARRAY:
                        inArray = true;
                        break;
                    case END_ARRAY:
                        inArray = false;
                        keyName = null;
                        customersVO = null;
                        break;
                    case START_OBJECT:
                        customersVO = inArray ? new CustomersVO(): null;
                        break;
                    case END_OBJECT:
                        if(customersVO != null && inArray)
                            list.add(customersVO);
                        customersVO = null;
                        keyName = null;
                        break;
                    case KEY_NAME:
                            switch(parser.getString()) {
                                case "customerName":
                                    keyName = "customerName";
                                    break;
                                case "customerAccount":
                                    keyName = "customerAccount";
                                    break;
                                case "fqdn":
                                    keyName = "fqdn";
                                    break;
                                case "logo":
                                    keyName = "logo";
                                    break;
                                case "skin":
                                    keyName = "skin";
                                    break;
                                case "sloUrl":
                                    keyName = "sloUrl";
                                    break;
                                default:
                                    keyName = null;
                                    break;      
                            }
                        break;
                    case VALUE_STRING:
                        if(customersVO != null && inArray && keyName != null) {
                            switch(keyName) {
                                case "customerName":
                                    customersVO.setCustomerName(parser.getString());
                                    break;
                                case "customerAccount":
                                    customersVO.setCustomerAccount(parser.getString());
                                    break;
                                case "fqdn":
                                    customersVO.setFqdn(parser.getString());
                                    break;
                                case "logo":
                                    customersVO.setLogo(parser.getString());
                                    break;
                                case "skin":
                                    customersVO.setSkin(parser.getString());
                                    break;
                                case "sloUrl":
                                    customersVO.setSloUrl(parser.getString());
                                    break;
                            }
                        }
                        break;
                }
            }
        } catch (Exception e) {
            Logger.getLogger(CustomerSaxJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }
}
