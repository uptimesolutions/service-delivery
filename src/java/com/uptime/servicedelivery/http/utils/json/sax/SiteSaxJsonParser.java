/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.servicedelivery.http.utils.json.sax;

import com.uptime.client.utils.SaxJsonParser;
import com.uptime.servicedelivery.utils.vo.SiteContactVO;
import com.uptime.servicedelivery.utils.vo.SiteVO;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;

/**
 *
 * @author kpati
 */
public class SiteSaxJsonParser extends SaxJsonParser {
    private static SiteSaxJsonParser instance = null;
    
    /**
     * Private Singleton class
     */
    private SiteSaxJsonParser() {
    }

    /**
     * Returns an instance of the class
     * @return SiteSaxJsonParser object
     */
    public static SiteSaxJsonParser getInstance() {
        if (instance == null) {
            instance = new SiteSaxJsonParser();
        }
        return instance; 
    }
    
    /**
     * Converts a json to a List Object of SiteVO Objects
     * @param content, String Object, json 
     * @return List Object of SiteVO Objects
     */
    public List<SiteVO> populateSiteFromJson(String content) {
        List<SiteVO> list = new ArrayList();
        String keyName;
        SiteVO siteVO;
        boolean inArray;
         
        try {
            parser = Json.createParser(new StringReader(content));
            keyName = null;
            siteVO = null;
            inArray = false;
            
            while (parser.hasNext()) {
                switch((event = parser.next())) {
                    case START_ARRAY:
                        inArray = true;
                        break;
                    case END_ARRAY:
                        inArray = false;
                        keyName = null;
                        siteVO = null;
                        break;
                    case START_OBJECT:
                        siteVO = inArray ? new SiteVO() : null;
                        break;
                    case END_OBJECT:
                        if(siteVO != null && inArray)
                            list.add(siteVO);
                        siteVO = null;
                        keyName = null;
                        break;
                    case KEY_NAME:
                            switch(parser.getString()) {
                                case "customerAccount":
                                    keyName = "customerAccount";
                                    break;
                                case "siteId":
                                    keyName = "siteId";
                                    break;
                                case "siteName":
                                    keyName = "siteName";
                                    break;
                                case "region":
                                    keyName = "region";
                                    break;
                                case "country":
                                    keyName = "country";
                                    break;
                                case "physicalAddress1":
                                    keyName = "physicalAddress1";
                                    break;
                                case "physicalAddress2":
                                    keyName = "physicalAddress2";
                                    break;
                                case "physicalCity":
                                    keyName = "physicalCity";
                                    break;
                                case "physicalStateProvince":
                                    keyName = "physicalStateProvince";
                                    break;
                                case "physicalPostalCode":
                                    keyName = "physicalPostalCode";
                                    break;
                                case "latitude":
                                    keyName = "latitude";
                                    break;
                                case "longitude":
                                    keyName = "longitude";
                                    break;
                                case "timezone":
                                    keyName = "timezone";
                                    break;
                                case "shipAddress1":
                                    keyName = "shipAddress1";
                                    break;
                                case "shipAddress2":
                                    keyName = "shipAddress2";
                                    break;
                                case "shipCity":
                                    keyName = "shipCity";
                                    break;
                                case "shipStateProvince":
                                    keyName = "shipStateProvince";
                                    break;
                                case "shipPostalCode":
                                    keyName = "shipPostalCode";
                                    break;
                                case "billingAddress1":
                                    keyName = "billingAddress1";
                                    break;
                                case "billingAddress2":
                                    keyName = "billingAddress2";
                                    break;
                                case "billingCity":
                                    keyName = "billingCity";
                                    break;
                                case "billingStateProvince":
                                    keyName = "billingStateProvince";
                                    break;
                                case "billingPostalCode":
                                    keyName = "billingPostalCode";
                                    break;
                                case "industry":
                                    keyName = "industry";
                                    break;
                                default:
                                    keyName = null;
                                    break;      
                            }
                        break;
                    case VALUE_STRING:
                        if(siteVO != null && inArray && keyName != null) {
                            switch(keyName) {
                                case "customerAccount":
                                    siteVO.setCustomerAccount(parser.getString());
                                    break;
                                case "siteId":
                                    siteVO.setSiteId(UUID.fromString(parser.getString()));
                                    break;
                                case "siteName":
                                    siteVO.setSiteName(parser.getString());
                                    break;
                                case "region":
                                    siteVO.setRegion(parser.getString());
                                    break;
                                case "country":
                                    siteVO.setCountry(parser.getString());
                                    break;
                                case "physicalAddress1":
                                    siteVO.setPhysicalAddress1(parser.getString());
                                    break;
                                case "physicalAddress2":
                                    siteVO.setPhysicalAddress2(parser.getString());
                                    break;
                                case "physicalCity":
                                    siteVO.setPhysicalCity(parser.getString());
                                    break;
                                case "physicalStateProvince":
                                    siteVO.setPhysicalStateProvince(parser.getString());
                                    break;
                                case "physicalPostalCode":
                                    siteVO.setPhysicalPostalCode(parser.getString());
                                    break;
                                case "latitude":
                                    siteVO.setLatitude(parser.getString());
                                    break;
                                case "longitude":
                                    siteVO.setLongitude(parser.getString());
                                    break;
                                case "timezone":
                                    siteVO.setTimezone(parser.getString());
                                    break;
                                case "shipAddress1":
                                    siteVO.setShipAddress1(parser.getString());
                                    break;
                                case "shipAddress2":
                                    siteVO.setShipAddress2(parser.getString());
                                    break;
                                case "shipCity":
                                    siteVO.setShipCity(parser.getString());
                                    break;
                                case "shipStateProvince":
                                    siteVO.setShipStateProvince(parser.getString());
                                    break;
                                case "shipPostalCode":
                                    siteVO.setShipPostalCode(parser.getString());
                                    break;
                                case "billingAddress1":
                                    siteVO.setBillingAddress1(parser.getString());
                                    break;
                                case "billingAddress2":
                                    siteVO.setBillingAddress2(parser.getString());
                                    break;
                                case "billingCity":
                                    siteVO.setBillingCity(parser.getString());
                                    break;
                                case "billingStateProvince":
                                    siteVO.setBillingStateProvince(parser.getString());
                                    break;
                                case "billingPostalCode":
                                    siteVO.setBillingPostalCode(parser.getString());
                                    break;
                                case "industry":
                                    siteVO.setIndustry(parser.getString());
                                    break;
                            }
                        }
                        break;
                }
            }
        } catch (Exception e) {
            Logger.getLogger(SiteSaxJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }
    
    
     /**
     * Converts a json to a List Object of SiteContactVO Objects
     * @param content, String Object, json 
     * @return List Object of SiteContactVO Objects
     */
    public List<SiteContactVO> populateSiteContactsFromJson(String content) {
        List<SiteContactVO> list = new ArrayList();
        String keyName;
        SiteContactVO siteContactVO;
        boolean inArray;
         
        try {
            parser = Json.createParser(new StringReader(content));
            keyName = null;
            siteContactVO = null;
            inArray = false;
            
            while (parser.hasNext()) {
                switch((event = parser.next())) {
                    case START_ARRAY:
                        inArray = true;
                        break;
                    case END_ARRAY:
                        inArray = false;
                        keyName = null;
                        siteContactVO = null;
                        break;
                    case START_OBJECT:
                        siteContactVO = inArray ? new SiteContactVO() : null;
                        break;
                    case END_OBJECT:
                        if(siteContactVO != null && inArray)
                            list.add(siteContactVO);
                        siteContactVO = null;
                        keyName = null;
                        break;
                    case KEY_NAME:
                            switch(parser.getString()) {
                                case "customerAccount":
                                    keyName = "customerAccount";
                                    break;
                                case "siteId":
                                    keyName = "siteId";
                                    break;
                                case "siteName":
                                    keyName = "siteName";
                                    break;
                                case "role":
                                    keyName ="role";
                                    break;
                                case "contactName":
                                    keyName ="contactName";
                                    break;
                                case "contactTitle":
                                    keyName ="contactTitle";
                                    break;
                                case "contactPhone":
                                    keyName ="contactPhone";
                                    break;
                                case "contactEmail":
                                    keyName ="contactEmail";
                                    break;
                                default:
                                    keyName = null;
                                    break;      
                            }
                        break;
                    case VALUE_STRING:
                        if(siteContactVO != null && inArray && keyName != null) {
                            switch(keyName) {
                                case "customerAccount":
                                    siteContactVO.setCustomerAccount(parser.getString());
                                    break;           
                                case "siteId":
                                    siteContactVO.setSiteId(parser.getString());
                                    break;
                                case "siteName":
                                    siteContactVO.setSiteName(parser.getString());
                                    break;
                                case "role":
                                    siteContactVO.setRole(parser.getString());
                                    break;
                                case "contactName":
                                    siteContactVO.setContactName(parser.getString());
                                    break;
                                case "contactTitle":
                                    siteContactVO.setContactTitle(parser.getString());
                                    break;
                                case "contactPhone":
                                    siteContactVO.setContactPhone(parser.getString());
                                    break;
                                case "contactEmail":
                                    siteContactVO.setContactEmail(parser.getString());
                                    break;                                
                            }
                        }
                        break;
                }
            }
        } catch (Exception e) {
            Logger.getLogger(SiteSaxJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }
    
    
}
