/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.servicedelivery.http.utils.json.dom;

import com.uptime.client.utils.DomJsonParser;
import com.uptime.client.utils.JsonConverterUtil;
import com.uptime.servicedelivery.utils.vo.SiteCustomerVO;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author twilcox
 */
public class ConfigDomJsonParser extends DomJsonParser {

    private static ConfigDomJsonParser instance = null;

    /**
     * Private Singleton class
     */
    private ConfigDomJsonParser() {
    }

    /**
     * Returns an instance of the class
     *
     * @return ConfigDomJsonParser object
     */
    public static ConfigDomJsonParser getInstance() {
        if (instance == null) {
            instance = new ConfigDomJsonParser();
        }
        return instance;
    }

    /**
     * Convert a SiteCustomerVO Object to a String Object json
     *
     * @param siteCustomerVO, SiteCustomerVO Object
     * @return String Object
     */
    public String getJsonFromConfig(SiteCustomerVO siteCustomerVO) {
        try {
            if (siteCustomerVO != null) {
                return JsonConverterUtil.toJSON(siteCustomerVO);
            }
        } catch (Exception e) {
            Logger.getLogger(ConfigDomJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return null;
    }
}
