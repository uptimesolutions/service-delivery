/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.servicedelivery.http.utils.json.dom;

import com.uptime.client.utils.DomJsonParser;
import com.uptime.client.utils.JsonConverterUtil;
import com.uptime.servicedelivery.utils.vo.SiteVO;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;

/**
 *
 * @author kpati
 */
public class SiteDomJsonParser extends DomJsonParser {

    private static SiteDomJsonParser instance = null;

    /**
     * Private Singleton class
     */
    private SiteDomJsonParser() {
    }

    /**
     * Returns an instance of the class
     *
     * @return SiteDomJsonParser object
     */
    public static SiteDomJsonParser getInstance() {
        if (instance == null) {
            instance = new SiteDomJsonParser();
        }
        return instance;
    }

    /**
     * Convert a SiteVO Object to a String Object json
     *
     * @param siteVO, SiteVO Object
     * @return String Object
     */
    public String getJsonFromSite(SiteVO siteVO) {
        try {
            if (siteVO != null) {
                return JsonConverterUtil.toJSON(siteVO);
            }
        } catch (Exception e) {
            //Logger.getLogger(SiteDomJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return null;
    }

    /**
     * Converts a json to an Object of SiteVO Objects
     *
     * @param content, String Object, json
     * @return List Object of SiteVO Objects
     */
    public SiteVO populateSiteVOFromGeocodeJson(String content) {
        SiteVO siteVO = null;
        if (content != null && !content.isEmpty()) {
            siteVO = new SiteVO();
            try (JsonReader jsonReader = Json.createReader(new StringReader(content))) {

                JsonObject jsonObject = jsonReader.readObject();
                jsonReader.close();
                JsonArray resultArray = jsonObject.getJsonArray("results");
                JsonObject resultObject = resultArray.getJsonObject(0);
                JsonArray locArray = resultObject.getJsonArray("locations");
                JsonObject locObject = locArray.getJsonObject(0);

                JsonObject latLangObject = locObject.getJsonObject("latLng");
                String latStr = latLangObject.getJsonNumber("lat").toString();
                String longStr = latLangObject.getJsonNumber("lng").toString();
                
                siteVO.setLatitude(latStr);
                siteVO.setLongitude(longStr);

            } catch (Exception e) {
                Logger.getLogger(SiteDomJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
            }
        }
        return siteVO;
    }

}
