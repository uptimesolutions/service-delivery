/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.servicedelivery.http.utils.json.sax;

import com.uptime.client.utils.SaxJsonParser;
import com.uptime.servicedelivery.utils.vo.SiteCustomerVO;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;

/**
 *
 * @author twilcox
 */
public class ConfigSaxJsonParser extends SaxJsonParser {
    private static ConfigSaxJsonParser instance = null;
    
    /**
     * Private Singleton class
     */
    private ConfigSaxJsonParser() {
    }

    /**
     * Returns an instance of the class
     * @return ConfigSaxJsonParser object
     */
    public static ConfigSaxJsonParser getInstance() {
        if (instance == null) {
            instance = new ConfigSaxJsonParser();
        }
        return instance; 
    }

    /**
     * Converts a json to a List Object of ApprovedBaseStationsVO Objects
     * @param content, String Object, json 
     * @return List Object of ApAlSetVO Objects
     */
    public List<SiteCustomerVO> populateSiteCustomerFromJson(String content) {
        List<SiteCustomerVO> list = new ArrayList();
        String keyName;
        SiteCustomerVO siteCustomerVO;
        boolean inArray;
         
        try {
            parser = Json.createParser(new StringReader(content));
            keyName = null;
            siteCustomerVO = null;
            inArray = false;
            
            while (parser.hasNext()) {
                switch((event = parser.next())) {
                    case START_ARRAY:
                        inArray = true;
                        break;
                    case END_ARRAY:
                        inArray = false;
                        keyName = null;
                        siteCustomerVO = null;
                        break;
                    case START_OBJECT:
                        siteCustomerVO = inArray ? new SiteCustomerVO(): null;
                        break;
                    case END_OBJECT:
                        if(siteCustomerVO != null && inArray)
                            list.add(siteCustomerVO);
                        siteCustomerVO = null;
                        keyName = null;
                        break;
                    case KEY_NAME:
                        switch(parser.getString()) {
                            case "customerAccount":
                                keyName = "customerAccount";
                                break;
                            case "siteId":
                                keyName = "siteId";
                                break;
                            case "siteName":
                                keyName = "siteName";
                                break;
                            case "region":
                                keyName = "region";
                                break;
                            case "country":
                                keyName = "country";
                                break;
                            case "physicalAddress1":
                                keyName = "physicalAddress1";
                                break;
                            case "physicalAddress2":
                                keyName = "physicalAddress2";
                                break;
                            case "physicalCity":
                                keyName = "physicalCity";
                                break;
                            case "physicalStateProvince":
                                keyName = "physicalStateProvince";
                                break;
                            case "physicalPostalCode":
                                keyName = "physicalPostalCode";
                                break;
                            case "latitude":
                                keyName = "latitude";
                                break;
                            case "longitude":
                                keyName = "longitude";
                                break;
                            case "timezone":
                                keyName = "timezone";
                                break;
                            case "shipAddress1":
                                keyName = "shipAddress1";
                                break;
                            case "shipAddress2":
                                keyName = "shipAddress2";
                                break;
                            case "shipCity":
                                keyName = "shipCity";
                                break;
                            case "shipStateProvince":
                                keyName = "shipStateProvince";
                                break;
                            case "shipPostalCode":
                                keyName = "shipPostalCode";
                                break;
                            case "billingAddress1":
                                keyName = "billingAddress1";
                                break;
                            case "billingAddress2":
                                keyName = "billingAddress2";
                                break;
                            case "billingCity":
                                keyName = "billingCity";
                                break;
                            case "billingStateProvince":
                                keyName = "billingStateProvince";
                                break;
                            case "billingPostalCode":
                                keyName = "billingPostalCode";
                                break;
                            case "industry":
                                keyName = "industry";
                                break;
                            default:
                                keyName = null;
                                break;      
                        }
                        break;
                    case VALUE_STRING:
                        if(siteCustomerVO != null && inArray && keyName != null) {
                            switch(keyName) {
                                case "customerAccount":
                                    siteCustomerVO.setCustomerAccount(parser.getString());
                                    break;
                                case "siteId":
                                    siteCustomerVO.setSiteId(UUID.fromString(parser.getString()));
                                    break;
                                case "siteName":
                                    siteCustomerVO.setSiteName(parser.getString());
                                    break;
                                case "region":
                                    siteCustomerVO.setRegion(parser.getString());
                                    break;
                                case "country":
                                    siteCustomerVO.setCountry(parser.getString());
                                    break;
                                case "physicalAddress1":
                                    siteCustomerVO.setPhysicalAddress1(parser.getString());
                                    break;
                                case "physicalAddress2":
                                    siteCustomerVO.setPhysicalAddress2(parser.getString());
                                    break;
                                case "physicalCity":
                                    siteCustomerVO.setPhysicalCity(parser.getString());
                                    break;
                                case "physicalStateProvince":
                                    siteCustomerVO.setPhysicalStateProvince(parser.getString());
                                    break;
                                case "physicalPostalCode":
                                    siteCustomerVO.setPhysicalPostalCode(parser.getString());
                                    break;
                                case "latitude":
                                    siteCustomerVO.setLatitude(parser.getString());
                                    break;
                                case "longitude":
                                    siteCustomerVO.setLongitude(parser.getString());
                                    break;
                                case "timezone":
                                    siteCustomerVO.setTimezone(parser.getString());
                                    break;
                                case "shipAddress1":
                                    siteCustomerVO.setShipAddress1(parser.getString());
                                    break;
                                case "shipAddress2":
                                    siteCustomerVO.setShipAddress2(parser.getString());
                                    break;
                                case "shipCity":
                                    siteCustomerVO.setShipCity(parser.getString());
                                    break;
                                case "shipStateProvince":
                                    siteCustomerVO.setShipStateProvince(parser.getString());
                                    break;
                                case "shipPostalCode":
                                    siteCustomerVO.setShipPostalCode(parser.getString());
                                    break;
                                case "billingAddress1":
                                    siteCustomerVO.setBillingAddress1(parser.getString());
                                    break;
                                case "billingAddress2":
                                    siteCustomerVO.setBillingAddress2(parser.getString());
                                    break;
                                case "billingCity":
                                    siteCustomerVO.setBillingCity(parser.getString());
                                    break;
                                case "billingStateProvince":
                                    siteCustomerVO.setBillingStateProvince(parser.getString());
                                    break;
                                case "billingPostalCode":
                                    siteCustomerVO.setBillingPostalCode(parser.getString());
                                    break;
                                case "industry":
                                    siteCustomerVO.setIndustry(parser.getString());
                                    break;
                            }
                        }
                        break;
                }
            }
        } catch (Exception e) {
            Logger.getLogger(ConfigSaxJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }
}
