/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.servicedelivery.http.utils.json.sax;

import com.uptime.client.utils.SaxJsonParser;
import com.uptime.servicedelivery.utils.vo.ApprovedBaseStationsVO;
import com.uptime.servicedelivery.utils.vo.EchoDiagnosticsVO;
import com.uptime.servicedelivery.utils.vo.MistDiagnosticsVO;
import java.io.StringReader;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;

/**
 *
 * @author twilcox
 */
public class MonitoringSaxJsonParser extends SaxJsonParser {
    private static MonitoringSaxJsonParser instance = null;
    
    /**
     * Private Singleton class
     */
    private MonitoringSaxJsonParser() {
    }

    /**
     * Returns an instance of the class
     * @return MonitoringSaxJsonParser object
     */
    public static MonitoringSaxJsonParser getInstance() {
        if (instance == null) {
            instance = new MonitoringSaxJsonParser();
        }
        return instance; 
    }
    
    public List<ApprovedBaseStationsVO> populateApprovedBaseStationsFromJson(String content) {
        List<ApprovedBaseStationsVO> list = new ArrayList();
        String keyName;
        ApprovedBaseStationsVO approvedBaseStationsVO;
        boolean inArray;
         
        try {
            parser = Json.createParser(new StringReader(content));
            keyName = null;
            approvedBaseStationsVO = null;
            inArray = false;
            
            while (parser.hasNext()) {
                switch((event = parser.next())) {
                    case START_ARRAY:
                        inArray = true;
                        break;
                    case END_ARRAY:
                        inArray = false;
                        keyName = null;
                        approvedBaseStationsVO = null;
                        break;
                    case START_OBJECT:
                        approvedBaseStationsVO = inArray ? new ApprovedBaseStationsVO(): null;
                        break;
                    case END_OBJECT:
                        if(approvedBaseStationsVO != null && inArray)
                            list.add(approvedBaseStationsVO);
                        approvedBaseStationsVO = null;
                        keyName = null;
                        break;
                    case KEY_NAME:
                            switch(parser.getString()) {
                                case "customerAccount":
                                    keyName = "customerAccount";
                                    break;
                                case "siteId":
                                    keyName = "siteId";
                                    break;
                                case "macAddress":
                                    keyName = "macAddress";
                                    break;
                                case "issuedToken":
                                    keyName = "issuedToken";
                                    break;
                                case "lastPasscode":
                                    keyName = "lastPasscode";
                                    break;
                                case "passcode":
                                    keyName = "passcode";
                                    break;
                                default:
                                    keyName = null;
                                    break;      
                            }
                        break;
                    case VALUE_STRING:
                        if(approvedBaseStationsVO != null && inArray && keyName != null) {
                            switch(keyName) {
                                case "customerAccount":
                                    approvedBaseStationsVO.setCustomerAccount(parser.getString());
                                    break;
                                case "siteId":
                                    try {
                                        approvedBaseStationsVO.setSiteId(UUID.fromString(parser.getString()));
                                    }catch (Exception ex) {}
                                    break;
                                case "macAddress":
                                    approvedBaseStationsVO.setMacAddress(parser.getString());
                                    break;
                                case "issuedToken":
                                    try {
                                        approvedBaseStationsVO.setIssuedToken(UUID.fromString(parser.getString()));
                                    }catch (Exception ex) {}
                                    break;
                                case "lastPasscode":
                                    try {
                                        approvedBaseStationsVO.setLastPasscode(LocalDateTime.parse(parser.getString(), DateTimeFormatter.ofPattern(DATE_TIME_OFFSET_FORMAT)).atZone(ZoneOffset.UTC).toInstant());
                                    }catch (Exception ex) {}
                                    break;
                                case "passcode":
                                    try {
                                        approvedBaseStationsVO.setPasscode(UUID.fromString(parser.getString()));
                                    }catch (Exception ex) {}
                                    break;
                            }
                        }
                        break;
                }
            }
        } catch (Exception e) {
            Logger.getLogger(MonitoringSaxJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }


    /**
     * Converts a json to a List Object of EchoDiagnosticsVO Objects
     * @param content, String Object, json 
     * @return List Object of EchoDiagnosticsVO Objects
     */
    public List<EchoDiagnosticsVO> populateEchoDiagnosFromJson(String content) {
        List<EchoDiagnosticsVO> list = new ArrayList();
        String keyName;
        EchoDiagnosticsVO echoDiagnosticsVO;
        boolean inArray;
         
        try {
            parser = Json.createParser(new StringReader(content));
            keyName = null;
            echoDiagnosticsVO = null;
            inArray = false;
            
            while (parser.hasNext()) {
                switch((event = parser.next())) {
                    case START_ARRAY:
                        inArray = true;
                        break;
                    case END_ARRAY:
                        inArray = false;
                        keyName = null;
                        echoDiagnosticsVO = null;
                        break;
                    case START_OBJECT:
                        echoDiagnosticsVO = inArray ? new EchoDiagnosticsVO(): null;
                        break;
                    case END_OBJECT:
                        if(echoDiagnosticsVO != null && inArray)
                            list.add(echoDiagnosticsVO);
                        echoDiagnosticsVO = null;
                        keyName = null;
                        break;
                    case KEY_NAME:
                            switch(parser.getString()) {
                                case "customerAccount":
                                    keyName = "customerAccount";
                                    break;
                                case "siteId":
                                    keyName = "siteId";
                                    break;
                                case "baseStationHostname":
                                    keyName = "baseStationHostname";
                                    break;
                                case "deviceId":
                                    keyName = "deviceId";
                                    break;
                                case "baseStationMac":
                                    keyName = "baseStationMac";
                                    break;
                                case "echoFw":
                                    keyName = "echoFw";
                                    break;
                                case "echoDatasets":
                                    keyName = "echoDatasets";
                                    break;
                                case "echoBacklog":
                                    keyName = "echoBacklog";
                                    break;
                                case "echoRadioFail":
                                    keyName = "echoRadioFail";
                                    break;
                                case "echoTerr":
                                    keyName = "echoTerr";
                                    break;
                                case "echoRestart":
                                    keyName = "echoRestart";
                                    break;
                                case "echoPowLevel":
                                    keyName = "echoPowLevel";
                                    break;
                                case "echoReboot":
                                    keyName = "echoReboot";
                                    break;
                                case "echoPowerfault":
                                    keyName = "echoPowerfault";
                                    break;
                                case "lastUpdate":
                                    keyName = "lastUpdate";
                                    break;
                                case "siteName":
                                    keyName = "siteName";
                                    break;
                                default:
                                    keyName = null;
                                    break;      
                            }
                        break;
                    case VALUE_STRING:
                        if(echoDiagnosticsVO != null && inArray && keyName != null) {
                            switch(keyName) {
                                case "customerAccount":
                                    echoDiagnosticsVO.setCustomerAccount(parser.getString());
                                    break;
                                case "siteId":
                                    try {
                                        echoDiagnosticsVO.setSiteId(UUID.fromString(parser.getString()));
                                    }catch (Exception ex) {}
                                    break;
                                case "siteName":
                                    echoDiagnosticsVO.setSiteName(parser.getString());
                                    break;
                                case "baseStationHostname":
                                    try {
                                        echoDiagnosticsVO.setBaseStationHostName(parser.getString());
                                    }catch (Exception ex) {}
                                    break;
                                case "deviceId":
                                    try {
                                        echoDiagnosticsVO.setDeviceId(parser.getString());
                                    }catch (Exception ex) {}
                                    break;
                                case "echoFw":
                                    try {
                                        echoDiagnosticsVO.setEchoFw(parser.getString());
                                    }catch (Exception ex) {}
                                    break;
                                case "baseStationMac":
                                    try {
                                        echoDiagnosticsVO.setBaseStationMac(parser.getString());
                                    }catch (Exception ex) {}
                                    break;
                                case "lastUpdate":
                                    try {
                                        echoDiagnosticsVO.setLastUpdate(LocalDateTime.parse(parser.getString(), DateTimeFormatter.ofPattern(DATE_TIME_OFFSET_FORMAT)).atZone(ZoneOffset.UTC).toInstant());
                                    }catch (Exception ex) {}
                                    break;
                            }
                        }
                        break;
                        case VALUE_NUMBER:
                        if (echoDiagnosticsVO != null && inArray && keyName != null) {
                            switch (keyName) {
                                case "echoDatasets":
                                    echoDiagnosticsVO.setEchoDatasets(parser.getBigDecimal().shortValue());
                                    break;
                                case "echoBacklog":
                                    echoDiagnosticsVO.setEchoBacklog(parser.getBigDecimal().shortValue());
                                    break;
                                case "echoReboot":
                                    echoDiagnosticsVO.setEchoReboot(parser.getBigDecimal().shortValue());
                                    break;
                                case "echoPowerfault":
                                    echoDiagnosticsVO.setEchoPowerfault(parser.getBigDecimal().shortValue());
                                    break;
                                case "echoRestart":
                                    echoDiagnosticsVO.setEchoRestart(parser.getBigDecimal().shortValue());
                                    break;
                                case "echoRadioFail":
                                    echoDiagnosticsVO.setEchoRadioFail(parser.getBigDecimal().byteValue());
                                    break;
                                case "echoTerr":
                                    echoDiagnosticsVO.setEchoTerr(parser.getBigDecimal().byteValue());
                                    break;
                                case "echoPowLevel":
                                    echoDiagnosticsVO.setEchoPowLevel(parser.getBigDecimal().byteValue());
                                    break;
                            }
                        }
                        break;
                }
            }
        } catch (Exception e) {
            Logger.getLogger(MonitoringSaxJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }
    
    /**
     * Converts a json to a List Object of EchoDiagnosticsVO Objects
     * @param content, String Object, json 
     * @return List Object of EchoDiagnosticsVO Objects
     */
    public List<MistDiagnosticsVO> populateMistDiagnosFromJson(String content) {
        List<MistDiagnosticsVO> list = new ArrayList();
        String keyName;
        MistDiagnosticsVO mistDiagnosticsVO;
        boolean inArray;
         
        try {
            parser = Json.createParser(new StringReader(content));
            keyName = null;
            mistDiagnosticsVO = null;
            inArray = false;
            
            while (parser.hasNext()) {
                switch((event = parser.next())) {
                    case START_ARRAY:
                        inArray = true;
                        break;
                    case END_ARRAY:
                        inArray = false;
                        keyName = null;
                        mistDiagnosticsVO = null;
                        break;
                    case START_OBJECT:
                        mistDiagnosticsVO = inArray ? new MistDiagnosticsVO(): null;
                        break;
                    case END_OBJECT:
                        if(mistDiagnosticsVO != null && inArray)
                            list.add(mistDiagnosticsVO);
                        mistDiagnosticsVO = null;
                        keyName = null;
                        break;
                    case KEY_NAME:
                            switch(parser.getString()) {
                                case "customerAccount":
                                    keyName = "customerAccount";
                                    break;
                                case "siteId":
                                    keyName = "siteId";
                                    break;
                                case "echoDeviceId":
                                    keyName = "echoDeviceId";
                                    break;
                                case "deviceId":
                                    keyName = "deviceId";
                                    break;
                                case "mistlxChan":
                                    keyName = "mistlxChan";
                                    break;
                                case "lastSample":
                                    keyName = "lastSample";
                                    break;
                                case "mistlxRssi":
                                    keyName = "mistlxRssi";
                                    break;
                                case "mistlxFsamp":
                                    keyName = "mistlxFsamp";
                                    break;
                                case "mistlxLength":
                                    keyName = "mistlxLength";
                                    break;
                                case "mistlxBattery":
                                    keyName = "mistlxBattery";
                                    break;
                                case "mistlxTemp":
                                    keyName = "mistlxTemp";
                                    break;
                                case "mistlxDelta":
                                    keyName = "mistlxDelta";
                                    break;
                                case "mistlxPcorr":
                                    keyName = "mistlxPcorr";
                                    break;
                                case "mistlxDbg":
                                    keyName = "mistlxDbg";
                                    break;
                                case "mistlxBleon":
                                    keyName = "mistlxBleon";
                                    break;
                                case "mistlxFpgaon":
                                    keyName = "mistlxFpgaon";
                                    break;
                                case "mistlxTotalMin":
                                    keyName = "mistlxTotalMin";
                                    break;
                                case "mistlxMincnt":
                                    keyName = "mistlxMincnt";
                                    break;
                                case "mistlxFailcnt":
                                    keyName = "mistlxFailcnt";
                                    break;
                                case "mistlxCtime":
                                    keyName = "mistlxCtime";
                                    break;
                                case "mistlxCtimeMod":
                                    keyName = "mistlxCtimeMod";
                                    break;
                                case "siteName":
                                    keyName = "siteName";
                                    break;
                                case "areaName":
                                    keyName = "areaName";
                                    break;
                                case "assetName":
                                    keyName = "assetName";
                                    break;
                                case "pointLocationName":
                                    keyName = "pointLocationName";
                                    break;
                                case "pointName":
                                    keyName = "pointName";
                                    break;
                                case "baseStationPort":
                                    keyName = "baseStationPort";
                                    break;
                                case "baseStationMac":
                                    keyName = "baseStationMac";
                                    break;
                                case "baseStationHostName":
                                    keyName = "baseStationHostName";
                                    break;
                                default:
                                    keyName = null;
                                    break;      
                            }
                        break;
                    case VALUE_STRING:
                        if(mistDiagnosticsVO != null && inArray && keyName != null) {
                            switch(keyName) {
                                case "customerAccount":
                                    mistDiagnosticsVO.setCustomerAccount(parser.getString());
                                    break;
                                case "siteId":
                                    try {
                                        mistDiagnosticsVO.setSiteId(UUID.fromString(parser.getString()));
                                    }catch (Exception ex) {}
                                    break;
                                case "echoDeviceId":
                                    mistDiagnosticsVO.setEchoDeviceId(parser.getString());
                                    break;
                                case "deviceId":
                                    try {
                                        mistDiagnosticsVO.setDeviceId(parser.getString());
                                    }catch (Exception ex) {}
                                    break;
                                case "lastSample":
                                    try {
                                        mistDiagnosticsVO.setLastSample(LocalDateTime.parse(parser.getString(), DateTimeFormatter.ofPattern(DATE_TIME_OFFSET_FORMAT)).atZone(ZoneOffset.UTC).toInstant());
                                    }catch (Exception ex) {}
                                    break;
                                case "siteName":
                                    try {
                                        mistDiagnosticsVO.setSiteName(parser.getString());
                                    }catch (Exception ex) {}
                                    break;
                                case "areaName":
                                    try {
                                        mistDiagnosticsVO.setAreaName(parser.getString());
                                    }catch (Exception ex) {}
                                    break;
                                case "assetName":
                                    try {
                                        mistDiagnosticsVO.setAssetName(parser.getString());
                                    }catch (Exception ex) {}
                                    break;
                                case "pointLocationName":
                                    try {
                                        mistDiagnosticsVO.setPointLocationName(parser.getString());
                                    }catch (Exception ex) {}
                                    break;
                                case "pointName":
                                    try {
                                        mistDiagnosticsVO.setPointName(parser.getString());
                                    }catch (Exception ex) {}
                                    break;
                                case "baseStationMac":
                                    try {
                                        mistDiagnosticsVO.setBaseStationMac(parser.getString());
                                    }catch (Exception ex) {}
                                    break;
                                case "baseStationHostName":
                                    try {
                                        mistDiagnosticsVO.setBaseStationHostName(parser.getString());
                                    }catch (Exception ex) {}
                                    break;
                                
                            }
                        }
                        break;
                        case VALUE_NUMBER:
                        if (mistDiagnosticsVO != null && inArray && keyName != null) {
                            switch (keyName) {
                                case "mistlxChan":
                                    mistDiagnosticsVO.setMistlxChan(parser.getBigDecimal().byteValue());
                                    break;
                                case "mistlxRssi":
                                    mistDiagnosticsVO.setMistlxRssi(parser.getBigDecimal().byteValue());
                                    break;
                                case "mistlxFsamp":
                                    mistDiagnosticsVO.setMistlxFsamp(parser.getBigDecimal().byteValue());
                                    break;
                                case "mistlxLength":
                                    mistDiagnosticsVO.setMistlxLength(parser.getBigDecimal().shortValue());
                                    break;
                                case "mistlxBattery":
                                    mistDiagnosticsVO.setMistlxBattery(parser.getBigDecimal().floatValue());
                                    break;
                                case "mistlxTemp":
                                    mistDiagnosticsVO.setMistlxTemp(parser.getBigDecimal().floatValue());
                                    break;
                                case "mistlxDelta":
                                    mistDiagnosticsVO.setMistlxDelta(parser.getBigDecimal().shortValue());
                                    break;
                                case "mistlxPcorr":
                                    mistDiagnosticsVO.setMistlxPcorr(parser.getBigDecimal().shortValue());
                                    break;
                                case "mistlxDbg":
                                    mistDiagnosticsVO.setMistlxDbg(parser.getBigDecimal().byteValue());
                                    break;
                                case "mistlxBleon":
                                    mistDiagnosticsVO.setMistlxBleon(parser.getBigDecimal().byteValue());
                                    break;
                                case "mistlxFpgaon":
                                    mistDiagnosticsVO.setMistlxFpgaon(parser.getBigDecimal().byteValue());
                                    break;
                                case "mistlxTotalMin":
                                    mistDiagnosticsVO.setMistlxTotalMin(parser.getBigDecimal().shortValue());
                                    break;
                                case "mistlxMincnt":
                                    mistDiagnosticsVO.setMistlxMincnt(parser.getBigDecimal().shortValue());
                                    break;
                                case "mistlxFailcnt":
                                    mistDiagnosticsVO.setMistlxFailcnt(parser.getBigDecimal().shortValue());
                                    break;
                                case "mistlxCtime":
                                    mistDiagnosticsVO.setMistlxCtime(parser.getBigDecimal().shortValue());
                                    break;
                                case "mistlxCtimeMod":
                                    mistDiagnosticsVO.setMistlxCtimeMod(parser.getBigDecimal().shortValue());
                                    break;
                                case "baseStationPort":
                                        mistDiagnosticsVO.setBaseStationPort(parser.getBigDecimal().shortValue());
                                    break;
                                
                            }
                        }
                        break;
                }
            }
        } catch (Exception e) {
            Logger.getLogger(MonitoringSaxJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }
    
}
