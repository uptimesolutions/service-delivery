/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.servicedelivery.http.utils.json.dom;

import com.uptime.client.utils.DomJsonParser;
import com.uptime.client.utils.JsonConverterUtil;
import com.uptime.servicedelivery.utils.vo.CustomersVO;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author twilcox
 */
public class CustomerDomJsonParser extends DomJsonParser {
    private static CustomerDomJsonParser instance = null;
    
    /**
     * Private Singleton class
     */
    private CustomerDomJsonParser() {
    }

    /**
     * Returns an instance of the class
     * @return CustomerDomJsonParser object
     */
    public static CustomerDomJsonParser getInstance() {
        if (instance == null) {
            instance = new CustomerDomJsonParser();
        }
        return instance; 
    }

    /**
     * Convert a CustomersVO Object to a String Object json
     * @param customersVO, CustomersVO Object
     * @return String Object
     */
    public String getJsonFromCustomer(CustomersVO customersVO) {
        try {
            if (customersVO != null) {
                return JsonConverterUtil.toJSON(customersVO);
            }
        } catch (Exception e) {
            Logger.getLogger(CustomerDomJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return null;
    }
}
