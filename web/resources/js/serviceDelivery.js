// Used to update the leftTreeFormId
function updateTreeNodes() {
    $('#leftTreeFormId').find('.ui-treenode-content').removeClass('treenode-selected-path');
    $('.ui-treenode-label.ui-state-highlight').parents('.ui-treenode').children('.ui-treenode-content').addClass('treenode-selected-path');
}